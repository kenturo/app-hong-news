import React, { Component } from 'react';
import { View } from 'react-native';
import { Flex, WhiteSpace, WingBlank, InputItem } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import get from 'lodash/get';

import styles from './Styles/FormProfileStyle';
import HelperFunc from '../Lib/helper';
import I18n from '../I18n';

export default class FormBank extends Component {
  render() {
    const { form, data } = this.props;
    const { getFieldProps } = form;

    return (
      <View>
        <WhiteSpace size="xl" />
        <WingBlank style={[styles.wingBlankStyle]}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="institution" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={[styles.input]}>
              <InputItem
                placeholder={I18n.t('placeHolder_bankName')}
                {...getFieldProps('bank_name', {
                  initialValue: get(data, 'bank_name', undefined),
                  rules: [
                    {
                      required: true,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_required',
                        'placeHolder_bankName'
                      ),
                    },
                    {
                      min: 1,
                      message: I18n.t('validate_len_bankName'),
                    },
                    {
                      WhiteSpace: false,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_whiteSpace',
                        'placeHolder_bankName'
                      ),
                    },
                  ],
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>
        <WhiteSpace size="xl" />
        <WingBlank style={[styles.wingBlankStyle]}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="user" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={[styles.input]}>
              <InputItem
                placeholder={I18n.t('placeHolder_accountName')}
                {...getFieldProps('account_name', {
                  initialValue: get(data, 'account_name', undefined),
                  rules: [
                    {
                      required: true,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_required',
                        'placeHolder_accountName'
                      ),
                    },
                    {
                      min: 1,
                      message: I18n.t('validate_len_accountName'),
                    },
                    {
                      WhiteSpace: false,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_whiteSpace',
                        'placeHolder_accountName'
                      ),
                    },
                  ],
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>
        <WhiteSpace size="xl" />
        <WingBlank style={styles.wingBlankStyle}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="credit-card" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={styles.input}>
              <InputItem
                placeholder={I18n.t('placeHolder_accountNumber')}
                {...getFieldProps('account_number', {
                  initialValue: get(data, 'account_number', undefined),
                  rules: [
                    {
                      required: true,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_required',
                        'placeHolder_accountNumber'
                      ),
                    },
                    {
                      min: 1,
                      message: I18n.t('validate_len_accountNumber'),
                    },
                    {
                      WhiteSpace: false,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_whiteSpace',
                        'placeHolder_accountNumber'
                      ),
                    },
                  ],
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>
      </View>
    );
  }
}
