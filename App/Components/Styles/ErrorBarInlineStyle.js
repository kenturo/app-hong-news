import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1
  },
  wingStyle: {
    backgroundColor: "#f4d8da",
    borderWidth: 1,
    borderColor: "#efcccf",
    borderRadius: 3
  },
  textStyle: {
    color: "#773639",
    fontSize: 18,
    paddingHorizontal: 15,
    paddingVertical: 10
  }
});
