import { StyleSheet } from 'react-native';
import { Colors, Metrics } from '../../Themes/';

export default StyleSheet.create({
  header: {
    backgroundColor: '#ed2939',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    height: 40,
  },
  text: {
    textAlign: 'center',
    color: '#fff',
  },
  row: {
    height: 40,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#0000000d',
  },
  itemPaging: {
    // width: 30,
    // height: 30,
    borderRightWidth: 0.3,
    borderTopWidth: 0.3,
    borderBottomWidth: 0.2,
    textAlign: 'center',
    paddingVertical: 4,
    paddingHorizontal: 10,
    color: Colors.MainTopHeader.backgroundColor,
  },
  itemPrev: {
    borderLeftWidth: 0.3,
  },
  itemNext: {
    borderRightWidth: 0.3,
  },

  itemPagingActive: {
    color: '#fff',
    backgroundColor: Colors.MainTopHeader.backgroundColor,
  },
});
