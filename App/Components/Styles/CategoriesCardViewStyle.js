import { StyleSheet, Dimensions } from 'react-native';

const wDimension = Dimensions.get('window').width / 3;

export default StyleSheet.create({
  flexOne: {
    flex: 1,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 15,
  },
  item: {
    width: wDimension,
    height: wDimension + 30,
    alignItems: 'center',
    marginVertical: 7,
    textAlign: 'center',
  },
  itemTxt: {
    textAlign: 'center',
    // color: "#fff"
  },
  itemImg: {
    flex: 1,
    height: wDimension - 14,
    width: wDimension - 14,
  },
});
