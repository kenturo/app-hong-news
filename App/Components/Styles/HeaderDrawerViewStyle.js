import { StyleSheet } from 'react-native';
import Color from '../../Themes/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    borderBottomWidth: 0.5,
  },
  textStyle: {
    color: Color.textColor,
    fontSize: 18,
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
});
