import { StyleSheet } from "react-native";
import { Colors } from "../../Themes/";

export default StyleSheet.create({
  container: {
    flex: 1,

    flexDirection: "row",
    paddingHorizontal: 5,
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 8
    // shadowColor: "#000",
    // shadowOffset: {
    //   width: 0,
    //   height: 2
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,

    // elevation: 5
  },
  borderTopHeader: {
    borderTopWidth: 3,
    borderTopColor: Colors.border
  },
  title: {
    color: "#ffa800",
    fontSize: 30
  },
  link: {
    fontSize: 20,
    color: Colors.linkColor
  }
});
