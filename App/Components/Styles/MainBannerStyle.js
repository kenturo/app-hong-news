import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    height: 200,
  },
  pagingContainerStyle: {
    width: Dimensions.get('window').width,
    backgroundColor: 'rgba(0, 0, 0, 0.55)',
    paddingVertical: 5,
    position: 'absolute',
    bottom: 0,
  },
  pagingDotStyle: {
    backgroundColor: 'rgba(255, 255, 255, 0.92)',
  },
  bannerItemStyle: {
    width: Dimensions.get('window').width,
    height: 200,
  },
  styleImage: {
    borderWidth: 1,
    borderColor: '#ffd200',
    margin: 6,
    width: Dimensions.get('window').width - 12,
  },
});
