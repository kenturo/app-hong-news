import { StyleSheet } from "react-native";
import { Colors } from "../../Themes/";

export default StyleSheet.create({
  flexOne: {
    flex: 1
  },
  container: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 5,
    flexWrap: "wrap",
    marginBottom: 15
  },
  item: {
    width: "32%",
    minHeight: 50,
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 7,
    backgroundColor: Colors.MainTabBottom.activeBackgroundColor,
    marginHorizontal: 2,
    paddingVertical: 10
  },
  itemTxt: {
    color: "#fff"
  },
  itemImg: {
    height: 40,
    width: 40
  }
});
