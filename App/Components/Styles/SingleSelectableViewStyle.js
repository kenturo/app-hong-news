import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 15,
    paddingHorizontal: 15,
    width: "100%"
  },
  pickerWraper: {
    flex: 1,
    height: 44.5,
    borderWidth: 1,
    borderColor: "#ddd",
    borderRadius: 5,
    paddingLeft: 10,
    justifyContent: "center"
  }
});
