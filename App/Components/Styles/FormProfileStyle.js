import { StyleSheet } from 'react-native';
// import { ApplicationStyles, Colors } from '../../Themes/';

export default StyleSheet.create({
  wingBlankStyle: {
    borderWidth: 1,
    borderColor: '#d3d8dc',
    borderRadius: 5,
  },
  disbaled: {
    backgroundColor: '#ced4da',
    color: '#000',
  },
  iconInput: {
    flex: 1,
    backgroundColor: '#eaecef',
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    flex: 6,
  },
});
