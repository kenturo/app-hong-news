import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Text } from "react-native";
import { Flex } from "@ant-design/react-native";
import styles from "./Styles/TitleScreenStyle";

export default class TitleScreen extends Component {
  // // Prop type warnings
  static propTypes = {
    value: PropTypes.string
  };

  // Defaults for props
  static defaultProps = {
    value: "Enter Title"
  };

  render() {
    return (
      <View style={styles.container}>
        <Flex direction="column">
          <Flex.Item>
            <Text
              style={styles.headerTitle}
              adjustsFontSizeToFit
              allowFontScaling
            >
              {this.props.value}
            </Text>
          </Flex.Item>
        </Flex>
      </View>
    );
  }
}
