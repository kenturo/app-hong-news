import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { List } from '@ant-design/react-native';
import get from 'lodash/get';
import request from '../Lib/request';
import Loading from './Loading';
import HelperFunc from '../Lib/helper';

import styles from './Styles/BankDepositeListViewStyle';

const Item = List.Item;

export default class BankDepositeListView extends Component {
  // // Prop type warnings
  static propTypes = {
    onChange: PropTypes.func,
  };
  //
  // // Defaults for props
  static defaultProps = {
    onChange: () => {},
  };

  state = {
    bankList: [],
    errorArr: [],
    isFetching: false,
    itemSelected: 'banking',
  };

  componentDidMount() {
    this.getBankList();
  }

  onChecked = itemSelected => {
    this.setState({ itemSelected });
  };

  getBankList = () => {
    this.setState({ isFetching: true });
    request.post('/gateway_payment').then(t => {
      const errorArr = get(t.data, 'messages', null);
      if (t.ok) {
        // check error code
        if (get(t.data, 'error_code', 0) > 0) {
          this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
        }

        this.setState({
          bankList: get(t.data, 'data', []),
        });
      } else {
        this.setState({ errorArr });
      }
      setTimeout(() => {
        this.setState({ isFetching: false });
      }, 2000);
    });
  };

  render() {
    const { bankList, errorArr, isFetching, itemSelected } = this.state;
    return (
      <View style={styles.container}>
        <Loading loading={isFetching}>
          {HelperFunc.getErrorFormForShow(errorArr)}
          <List>
            {bankList.map(t =>
              t && t.active ? (
                <Item
                  key={t.id}
                  thumb={t.logo}
                  arrow={itemSelected === t.id ? 'empty' : 'horizontal'}
                  onPress={() => {
                    this.onChecked(t.id);
                    this.props.onChange(t);
                  }}
                >
                  <Text style={itemSelected === t.id ? styles.actived : {}}>{t.name}</Text>
                </Item>
              ) : null
            )}
          </List>
        </Loading>
      </View>
    );
  }
}
