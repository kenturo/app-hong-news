import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native';
import { Picker, InputItem } from '@ant-design/react-native';
import get from 'lodash/get';

// import styles from './Styles/ChannelBankSelectedViewStyle';

export default class ChannelBankSelectedView extends Component {
  // // Prop type warnings
  static propTypes = {
    loading: PropTypes.bool,
  };

  // Defaults for props
  static defaultProps = {
    loading: false,
  };
  getLabel = key => {
    const { data } = this.props;
    if (data && Array.isArray(data)) {
      if (key) {
        const item = data.filter(t => key.includes(t.value));
        return get(item, '[0].label', 'Choose');
      }
    }

    return get(data, '[0].label', 'Choose');
  };

  render() {
    const { form, data, loading } = this.props;
    const CustomChildren = props => (
      <TouchableOpacity onPress={props.onPress}>{props.children}</TouchableOpacity>
    );
    return (
      <Picker {...form} okText="Ok" dismissText="Cancel" data={data} cols={1}>
        <CustomChildren>
          <InputItem
            style={{
              backgroundColor: '#fff',
              color: '#222',
            }}
            placeholder="hhh"
            disabled={true}
            value={loading && data && data.length === 0 ? 'Loading...' : this.getLabel(form.value)}
          ></InputItem>
        </CustomChildren>
      </Picker>
    );
  }
}
