import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, View, Text, Dimensions, TouchableOpacity } from 'react-native';
import { Flex, WingBlank, WhiteSpace, ActivityIndicator } from '@ant-design/react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import get from 'lodash/get';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './Styles/TableListStyle';
import I18n from '../I18n';

export default class TableList extends Component {
  // Prop type warnings
  static propTypes = {
    columnsHead: PropTypes.array.isRequired,
    widthArr: PropTypes.array.isRequired,
    heightArr: PropTypes.array.isRequired,
    isFetching: PropTypes.bool.isRequired,
    columnData: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
    paging: PropTypes.object.isRequired,
    onPagingChange: PropTypes.func,
    width: PropTypes.number.isRequired,
    styleWrapperRow: PropTypes.object,
  };

  // Defaults for props
  static defaultProps = {
    columnData: [],
    columnsHead: [],
    widthArr: [],
    heightArr: [],
    isFetching: false,
    data: [],
    paging: {},
    onPagingChange: () => {},
    width: 0,
    styleWrapperRow: null,
  };

  renderPagingTable = ({ isFetching, data, paging, onPagingChange }) => {
    let page = null;
    let numberOfPage = 1;

    if (data && data.length === 0) {
      return !isFetching ? (
        <Text
          style={{
            textAlign: 'center',
            fontSize: 18,
          }}
        >
          无记录
        </Text>
      ) : null;
    }

    if (paging && Object.keys(paging).length === 0) {
      page = <Text style={[styles.itemPaging, styles.itemPagingActive]}>1</Text>;
    } else {
      const { total, per_page, current_page } = paging;
      numberOfPage = Math.ceil(total / per_page);

      page = new Array(numberOfPage).fill(numberOfPage).map((t, k) => {
        if (current_page === k + 1) {
          return (
            <Text key={k} style={[styles.itemPaging, styles.itemPagingActive]}>
              {current_page}
            </Text>
          );
        }
        return (
          <TouchableOpacity key={k} onPress={() => onPagingChange({ page: k + 1 })}>
            <Text style={[styles.itemPaging]}>{k + 1}</Text>
          </TouchableOpacity>
        );
      });
    }

    return (
      <Flex align="center" justify="center">
        <TouchableOpacity
          onPress={() =>
            paging.current_page > 1 ? onPagingChange({ page: paging.current_page - 1 }) : null
          }
        >
          <Text style={[styles.itemPaging, styles.itemPrev]}>
            <Icon name="angle-double-left" size={18} />
          </Text>
        </TouchableOpacity>

        {page}

        <TouchableOpacity
          onPress={() =>
            paging.current_page < numberOfPage
              ? onPagingChange({ page: paging.current_page + 1 })
              : null
          }
        >
          <Text style={[styles.itemPaging, styles.itemNext]}>
            <Icon name="angle-double-right" size={18} />
          </Text>
        </TouchableOpacity>
      </Flex>
    );
  };

  render() {
    const {
      columnData,
      columnsHead,
      widthArr,
      heightArr,
      data,
      isFetching,
      width,
      styleWrapperRow,
    } = this.props;

    return (
      <View>
        <ScrollView horizontal={true}>
          <View>
            <Table
              borderStyle={{
                width,
              }}
            >
              <Row
                data={columnsHead}
                widthArr={widthArr}
                heightArr={heightArr}
                style={styles.header}
                textStyle={styles.text}
              />

              {data && data.length > 0 ? (
                data.map((rowData, index) => (
                  <TableWrapper
                    key={index}
                    style={[
                      styles.row,
                      {
                        width: width,
                      },
                      styleWrapperRow,
                    ]}
                  >
                    {columnData.map((cData, k) => (
                      <Cell
                        key={k}
                        data={cData.render(get(rowData, cData.key), rowData)}
                        textStyle={styles.text}
                      />
                    ))}
                  </TableWrapper>
                ))
              ) : (
                <TableWrapper />
              )}
            </Table>
          </View>
        </ScrollView>
        <WhiteSpace size="xl" />
        <ActivityIndicator animating={isFetching} size="large" text={I18n.t('label_loading')} />
        <WhiteSpace size="xl" />
        <WingBlank>
          <this.renderPagingTable {...this.props} />
        </WingBlank>
        <WhiteSpace size="xl" />
      </View>
    );
  }
}
