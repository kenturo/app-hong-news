import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { View, Image, Dimensions, TouchableOpacity } from 'react-native';
import HelperFunc from '../Lib/helper';
// import { connect } from 'react-redux';
import * as Sentry from '@sentry/react-native';

import Loading from './Loading';

import styles from './Styles/MainBannerStyle';
import request from '../Lib/request';

class MainBanner extends PureComponent {
  static propTypes = {
    renderRaw: PropTypes.bool,
    onClickMore: PropTypes.func,
  };
  //
  // // Defaults for props
  static defaultProps = {
    renderRaw: false,
  };

  state = {
    activeSlide: 0,
    bannerList: [],
    isFetching: false,
  };

  componentDidMount() {
    this.fetchBannerList();
  }

  fetchBannerList = () => {
    this.setState({
      isFetching: true,
    });
    try {
      request.get('/main_banners').then(t => {
        if (t.ok) {
          this.setState({
            bannerList: t.data.data,
            isFetching: false,
          });
        }
      });
    } catch (error) {
      Sentry.captureException(new Error(error));
    } finally {
      setTimeout(() => {
        this.setState({
          isFetching: false,
        });
      }, 1000);
    }
  };

  handleUrl = url => {
    const REGEX = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm;
    if (url && url.length) {
      const isMatch = url.match(REGEX);
      if (isMatch) {
        return isMatch[0];
      } else {
        return `${AppConfig.HOSTNAME_ENDPOINT}${url}`;
      }
    }
    return null;
  };

  _renderItem({ item }, styleItem = null) {
    return (
      <TouchableOpacity key={item.id} onPress={() => HelperFunc.loadInBrowser(item.url)}>
        <Image source={{ uri: item.image }} style={[styles.bannerItemStyle, styleItem]} />
      </TouchableOpacity>
    );
  }

  render() {
    const { bannerList, activeSlide, isFetching } = this.state;

    if (this.props.renderRaw) {
      return bannerList.map(t => this._renderItem({ item: t }, styles.styleImage));
    }

    return (
      <View style={styles.container}>
        <Loading loading={isFetching}>
          <Carousel
            loop={true}
            autoplay={true}
            data={bannerList}
            renderItem={r => this._renderItem(r)}
            sliderWidth={Dimensions.get('window').width}
            itemWidth={Dimensions.get('window').width}
            onSnapToItem={index => this.setState({ activeSlide: index })}
          />

          <Pagination
            dotsLength={bannerList.length}
            activeDotIndex={activeSlide}
            containerStyle={styles.pagingContainerStyle}
            dotStyle={styles.pagingDotStyle}
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
          />
        </Loading>
      </View>
    );
  }
}

// const mapDispatchToProps = dispatch => {
//   return {
//     onOpenWebView: data => {
//       dispatch(ModalWebViewAction.modalWebViewRequest(data));
//     },
//   };
// };

export default MainBanner;
