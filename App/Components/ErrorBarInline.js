import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Text } from "react-native";
import { WingBlank } from "@ant-design/react-native";
import styles from "./Styles/ErrorBarInlineStyle";

export default class ErrorBarInline extends Component {
  // // Prop type warnings
  static propTypes = {
    value: PropTypes.any
  };
  //
  // // Defaults for props
  static defaultProps = {
    value: null
  };

  render() {
    const { value } = this.props;
    if (!value) return null;
    return (
      <View style={styles.container}>
        <WingBlank style={styles.wingStyle}>
          <Text adjustsFontSizeToFit allowFontScaling style={styles.textStyle}>
            {this.props.value}
          </Text>
        </WingBlank>
      </View>
    );
  }
}
