import React, { useState, useEffect } from 'react';
// import PropTypes from 'prop-types';
import { View, Text, Image } from 'react-native';
import { Flex, DatePicker, WhiteSpace } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import I18n from '../I18n';
import moment from 'moment';
import styles from './Styles/SearchHistoriesStyle';
import { Images } from '../Themes';

const SearchHistories = ({ children, onChange }) => {
  const [rangeValue, setRangeValue] = useState({
    start: moment()
      .subtract(1, 'months')
      .toDate(),
    end: moment().toDate(),
  });

  useEffect(
    () =>
      onChange({
        start_date: moment(rangeValue.start).format('YYYY/MM/DD'),
        end_date: moment(rangeValue.end).format('YYYY/MM/DD'),
      }),
    [rangeValue]
  );

  return (
    <View>
      <View style={[styles.wingBlankStyle]}>
        <Flex justify="start">
          <Flex.Item style={styles.iconInput}>
            <Icon size={18} name="calendar" color="#6b6c6e" />
          </Flex.Item>
          <Flex.Item style={[styles.input]}>
            <DatePicker
              value={rangeValue.start}
              mode="date"
              defaultDate={new Date()}
              okText={I18n.t('label_ok')}
              dismissText={I18n.t('label_cancel')}
              minDate={new Date(2015, 7, 6)}
              maxDate={new Date(2026, 11, 3)}
              onChange={start => setRangeValue(Object.assign({}, rangeValue, { start }))}
              format="YYYY-MM-DD"
            >
              <Text>
                {rangeValue.start ? moment(rangeValue.start).format('YYYY/MM/DD') : 'Select Date'}
              </Text>
            </DatePicker>
          </Flex.Item>
        </Flex>
      </View>
      <WhiteSpace />
      <View style={[styles.wingBlankStyle]}>
        <Flex justify="start">
          <Flex.Item style={styles.iconInput}>
            <Icon size={18} name="calendar" color="#6b6c6e" />
          </Flex.Item>
          <Flex.Item style={[styles.input]}>
            <DatePicker
              value={rangeValue.end}
              mode="date"
              defaultDate={new Date()}
              okText={I18n.t('label_ok')}
              dismissText={I18n.t('label_cancel')}
              minDate={new Date(2015, 7, 6)}
              maxDate={new Date(2026, 11, 3)}
              onChange={end => setRangeValue(Object.assign({}, rangeValue, { end }))}
              format="YYYY-MM-DD"
            >
              <Text>
                {rangeValue.end ? moment(rangeValue.end).format('YYYY/MM/DD') : 'Select Date'}
              </Text>
            </DatePicker>
          </Flex.Item>
        </Flex>
      </View>
      <WhiteSpace />
      {children ? (
        <View style={[styles.wingBlankStyle]}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Image
                style={{
                  width: 24,
                  height: 24,
                  tintColor: '#6b6c6e',
                }}
                source={Images.icoQueen}
              />
            </Flex.Item>
            <Flex.Item style={[styles.input]}>{children}</Flex.Item>
          </Flex>
        </View>
      ) : null}
    </View>
  );
};
export default SearchHistories;
