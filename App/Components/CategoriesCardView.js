import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Toast, Portal } from '@ant-design/react-native';
import get from 'lodash/get';
import CardHeader from './CardHeader';
import BrandsCardView from './BrandsCardView';
import styles from './Styles/CategoriesCardViewStyle';
import request from '../Lib/request';
import HelperFunc from '../Lib/helper';
import Loading from './Loading';
import { TITLE_GAME_BY_TYPE } from '../Constants';
import I18n from '../I18n';
import * as Sentry from '@sentry/react-native';

export default class CategoriesCardView extends Component {
  state = {
    dataList: [],
    isFetching: false,
    isMore: false,
  };

  componentDidMount() {
    this.fetchData(this.props.type);
  }

  getEndpointByType = type => {
    switch (type) {
      case 'top-livecasinos':
        return '/game/top_livecasinos';
      case 'top-slots':
        return '/game/top_slots';
      case 'top-lotto':
        return '/game/top_lotteries';

      case 'livecasinos':
        return '/game/livecasinos';

      case 'slots':
        return '/game/slots';
      default:
        return '/game/top_sports';
    }
  };

  fetchData = type => {
    this.setState({
      isFetching: true,
    });
    try {
      const actions = this.getEndpointByType(type);
      const qstring = get(this.props.navigation, 'state.params.qstring', '');
      request.get(actions + qstring).then(t => {
        if (t.ok && t.data.error_code === 0) {
          this.setState({
            dataList: t.data.data,
            isMore: t.data.is_show_more,
          });
        }
      });
    } catch (error) {
      Sentry.captureException(new Error(error));
    } finally {
      setTimeout(() => {
        this.setState({
          isFetching: false,
        });
      }, 1000);
    }
  };

  onPlayGame = payload => {
    const toast = Toast.loading(I18n.t('label_loading'), 0);
    const fd = new FormData();
    Object.keys(payload).forEach(function(key) {
      fd.append(key, payload[key]);
    });

    try {
      request.post('/game/play', fd).then(t => {
        if (t.ok && get(t, 'data.error_code', -1) === 0) {
          const url = get(t, 'data.url', null);
          HelperFunc.loadInBrowser(url);
        } else {
          const mesg = get(t, 'data.message', 'Not found game link');
          Sentry.captureMessage(
            JSON.stringify({
              title: 'onPlayGame',
              mesg,
              payload,
            }),
            'debug'
          );
          Toast.fail(mesg, 1);
        }
        Portal.remove(toast);
      });
    } catch (error) {
      Sentry.captureException(new Error(error));
    }
  };

  renderItem = dataList => {
    const { token, navigation } = this.props;
    if (dataList && dataList.length === 0) {
      return null;
    }

    return dataList.map((t, k) => (
      <View style={styles.item} key={k}>
        <TouchableOpacity
          onPress={() => {
            if (!token) {
              // not have token (not yet login)
              navigation.navigate({
                routeName: 'LoginTab',
              });
            } else {
              // do something : play game
              this.onPlayGame(t);
            }
          }}
        >
          <Image
            source={{
              uri: t.image,
              cache: 'only-if-cached',
            }}
            progressiveRenderingEnabled={true}
            style={styles.itemImg}
            resizeMode="contain"
          />
          <Text style={[styles.itemTxt]}>{t.gameName}</Text>
        </TouchableOpacity>
      </View>
    ));
  };

  render() {
    const { hiddenBorder, type, navigation } = this.props;
    const { isFetching, isMore, dataList } = this.state;

    if (!Array.isArray(dataList)) {
      return (
        <BrandsCardView
          type={type}
          data={dataList}
          navigation={navigation}
          isFetching={isFetching}
          renderItem={this.renderItem}
        />
      );
    }

    return (
      <View style={styles.flexOne}>
        <Loading loading={isFetching}>
          <CardHeader
            title={TITLE_GAME_BY_TYPE[type]}
            paramMore={{ type }}
            isMore={isMore}
            hiddenBorder={hiddenBorder}
          />
          <View style={styles.container}>{this.renderItem(dataList)}</View>
        </Loading>
      </View>
    );
  }
}
