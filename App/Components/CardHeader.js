import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import styles from './Styles/CardHeaderStyle';
import { withNavigation } from 'react-navigation';

class CardHeader extends Component {
  static propTypes = {
    title: PropTypes.string,
    paramMore: PropTypes.object,
    hiddenBorder: PropTypes.bool,
    isMore: PropTypes.bool,
    onClickMore: PropTypes.func,
  };
  //
  // // Defaults for props
  static defaultProps = {
    title: '',
    paramMore: null,
    hiddenBorder: false,
    isMore: false,
    onClickMore: null,
  };

  render() {
    const { navigation, hiddenBorder, isMore, paramMore, title, onClickMore } = this.props;

    return (
      <View style={[styles.container, !hiddenBorder ? styles.borderTopHeader : {}]}>
        <Text style={styles.title}>{title}</Text>
        {isMore ? (
          <Text
            style={styles.link}
            onPress={() => {
              if (onClickMore && typeof onClickMore === 'function') {
                onClickMore();
              } else {
                navigation.push('CategoriesDetailListScreen', paramMore);
              }
            }}
          >
            更多
          </Text>
        ) : null}
      </View>
    );
  }
}

export default withNavigation(CardHeader);
