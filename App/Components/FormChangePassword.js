import React, { Component } from 'react';
import { View } from 'react-native';
import { Flex, WhiteSpace, WingBlank, InputItem } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import styles from './Styles/FormProfileStyle';
import HelperFunc from '../Lib/helper';
import I18n from '../I18n';

export default class FormChangePassword extends Component {
  compareToFirstPassword = (rule, value, callback) => {
    if (value && value !== this.props.form.getFieldValue('password')) {
      callback(I18n.t('validate_two_password'));
    } else {
      callback();
    }
  };

  render() {
    const { form, user } = this.props;
    const { getFieldProps } = form;

    return (
      <View>
        <WhiteSpace size="xl" />
        <WingBlank style={[styles.wingBlankStyle]}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="lock" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={[styles.input]}>
              <InputItem
                type="password"
                placeholder={I18n.t('placeHolder_oldPassword')}
                {...getFieldProps('old_password', {
                  rules: [
                    {
                      required: true,
                      message: HelperFunc.getMesgPlural('plural_field_required', 'label_password'),
                    },
                    {
                      WhiteSpace: false,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_whiteSpace',
                        'label_password'
                      ),
                    },
                    {
                      min: 6,
                      message: I18n.t('validate_len_password'),
                    },
                  ],
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>
        <WhiteSpace size="xl" />
        <WingBlank style={[styles.wingBlankStyle]}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="lock" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={[styles.input]}>
              <InputItem
                type="password"
                placeholder={I18n.t('placeHolder_newPassword')}
                {...getFieldProps('password', {
                  rules: [
                    {
                      required: true,
                      message: HelperFunc.getMesgPlural('plural_field_required', 'label_password'),
                    },
                    {
                      WhiteSpace: false,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_whiteSpace',
                        'label_password'
                      ),
                    },
                    {
                      min: 6,
                      message: I18n.t('validate_len_password'),
                    },
                  ],
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>
        <WhiteSpace size="xl" />
        <WingBlank style={styles.wingBlankStyle}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="lock" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={styles.input}>
              <InputItem
                type="password"
                placeholder={I18n.t('placeHolder_confirmNewPassword')}
                {...getFieldProps('confirm_password', {
                  rules: [
                    {
                      required: true,
                      message: HelperFunc.getMesgPlural('plural_field_required', 'label_password'),
                    },
                    { validator: this.compareToFirstPassword },
                  ],
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>
      </View>
    );
  }
}
