import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollView, Text, View, Dimensions } from 'react-native';
import styles from './Styles/MainLayoutHoCStyle';

import { Drawer, List, Button } from '@ant-design/react-native';
import LayoutContext from '../Config/layoutContext';

class MainLayoutHoC extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor() {
    super(...arguments);
    this.onOpenChange = isOpen => {
      /* tslint:disable: no-console */
      // console.log(' Drawer', isOpen.toString());
    };
  }

  onOpenDrawer = () => {
    this.drawer && this.drawer.openDrawer();
  };

  onCloseDrawer = () => {
    this.drawer && this.drawer.closeDrawer();
  };

  getContext() {
    const { token } = this.props;

    return {
      onOpenDrawer: this.onOpenDrawer,
      onCloseDrawer: this.onCloseDrawer,
      token,
    };
  }

  render() {
    const itemArr = Array.apply(null, Array(5))
      .map(function(_, i) {
        return i;
      })
      .map((_i, index) => {
        if (index === 0) {
          return (
            <List.Item key={index} multipleLine>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
              >
                <Text>Categories - {index}</Text>
                <Button type="primary" size="small" onPress={() => this.onCloseDrawer()}>
                  x
                </Button>
              </View>
            </List.Item>
          );
        }
        return (
          <List.Item
            key={index}
            thumb="https://zos.alipayobjects.com/rmsportal/eOZidTabPoEbPeU.png"
          >
            <Text>Categories - {index}</Text>
          </List.Item>
        );
      });

    const sidebar = (
      <ScrollView>
        <List>{itemArr}</List>
      </ScrollView>
    );

    return (
      <View style={styles.container}>
        <LayoutContext.Provider value={this.getContext()}>
          <Drawer
            sidebar={sidebar}
            position="left"
            open={false}
            drawerRef={el => (this.drawer = el)}
            onOpenChange={this.onOpenChange}
            drawerBackgroundColor="#ddd"
            drawerWidth={Dimensions.get('window').width}
          >
            {this.props.children({ ...this.props, onOpen: this.onOpenDrawer })}
          </Drawer>
        </LayoutContext.Provider>
      </View>
    );
  }
}

const mapStateToProps = state => {
  // console.log('TCL: state', state);

  return state.login;
};

const mapDispatchToProps = dispatch => {
  return {
    setTokenApp: token => {
      dispatch(LoginActions.setToken(token));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainLayoutHoC);
