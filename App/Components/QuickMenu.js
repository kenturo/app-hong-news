import React, { Component } from "react";
// import PropTypes from 'prop-types';
import { View, Text, Image } from "react-native";
import styles from "./Styles/QuickMenuStyle";

export default class QuickMenu extends Component {
  ArrayMenu = [
    {
      img: "https://008686.com/themes/mobile/img/ic_sabah.png",
      name: "体育",
      href: "https://008686.com/play/sport.html"
    },
    {
      img: "https://008686.com/themes/mobile/img/ic_ky.png",
      name: "真人娱乐",
      href: "https://008686.com/livecasino.html"
    },
    {
      img: "https://008686.com/themes/mobile/img/ic_pt.png",
      name: "电子游戏",
      href: "https://008686.com/slot.html"
    },
    ,
    {
      img: "https://008686.com/themes/mobile/img/ic_lottery.png",
      name: "彩票",
      href: "https://008686.com/play/lotto.html?product_type=2&game_code=SSC"
    },
    {
      img: "https://008686.com/themes/mobile/img/ic_liveshow.png",
      name: "优惠",
      href: "https://008686.com/promotions.html"
    },
    {
      img: "https://008686.com/themes/mobile/img/ic_livechat.png",
      name: "在线客服",
      href: "https://lc.chat/now/10753432/"
    }
  ];

  renderItem = () => {
    return this.ArrayMenu.map((t, k) => (
      <View style={styles.item} key={k}>
        <Image
          source={{
            uri: t.img
          }}
          style={[styles.flexOne, styles.itemImg]}
          resizeMode="contain"
        />
        <Text style={[styles.flexOne, styles.itemTxt]}>{t.name}</Text>
      </View>
    ));
  };

  render() {
    return <View style={styles.container}>{this.renderItem()}</View>;
  }
}
