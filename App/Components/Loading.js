import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, ActivityIndicator } from "react-native";
import styles from "./Styles/LoadingStyle";

export default class Loading extends Component {
  // // Prop type warnings
  static propTypes = {
    loading: PropTypes.bool.isRequired
  };
  //
  // // Defaults for props
  static defaultProps = {
    loading: false
  };

  render() {
    const { loading } = this.props;
    let styleView = styles.container;
    if (loading) {
      styleView = Object.assign({}, styleView, styles.overlay);
    } else {
      return this.props.children;
    }

    return (
      <View
        style={{
          ...styles.container,
          backgroundColor: loading ? "rgba(0, 0, 0, 0.35)" : null
        }}
      >
        <ActivityIndicator
          animating={loading}
          size="large"
          color="#ed2939"
          style={styles.indicator}
        />
      </View>
    );
  }
}
