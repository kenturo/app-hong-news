import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { View } from 'react-native';
import get from 'lodash/get';
import CardHeader from './CardHeader';
import styles from './Styles/CategoriesCardViewStyle';
import Loading from './Loading';
// import { TITLE_GAME_BY_TYPE } from "../Constants";

export default class BrandsCardView extends Component {
  render() {
    const { hiddenBorder, navigation, type, data, isFetching, renderItem } = this.props;

    return Object.keys(data).map(t => (
      <View style={styles.flexOne} key={t}>
        <Loading loading={isFetching}>
          <CardHeader
            title={t}
            isMore={get(data[t], 'is_show_more', false)}
            hiddenBorder={hiddenBorder}
            onClickMore={() => {
              navigation.push('CategoriesDetailListScreen', {
                type,
                qstring: `/${t}`,
                titleScreen: t,
              });
            }}
          />
          <View style={styles.container}>{renderItem(get(data[t], 'data', []))}</View>
        </Loading>
      </View>
    ));
  }
}
