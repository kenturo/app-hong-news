import React, { Component } from 'react';
import { View } from 'react-native';
import { Flex, WhiteSpace, WingBlank, InputItem } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import get from 'lodash/get';

import styles from './Styles/FormProfileStyle';
import HelperFunc from '../Lib/helper';
import I18n from '../I18n';

export default class FormProfile extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render() {
    const { form, user } = this.props;
    const { getFieldProps } = form;
    getFieldProps('id', {
      initialValue: get(user, 'id', undefined),
    });

    return (
      <View>
        <WhiteSpace size="xl" />
        <WingBlank style={[styles.wingBlankStyle, styles.disbaled]}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="user" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={[styles.input, styles.disbaled]}>
              <InputItem
                style={styles.disbaled}
                disabled={true}
                placeholder={I18n.t('placeHolder_username')}
                {...getFieldProps('username', {
                  initialValue: get(user, 'username', undefined),
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>
        <WhiteSpace size="xl" />
        <WingBlank style={[styles.wingBlankStyle, styles.disbaled]}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="envelope" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={[styles.input, styles.disbaled]}>
              <InputItem
                disabled={true}
                style={styles.disbaled}
                placeholder={I18n.t('placeHolder_email')}
                {...getFieldProps('email', {
                  initialValue: get(user, 'email', undefined),
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>
        <WhiteSpace size="xl" />
        <WingBlank style={styles.wingBlankStyle}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={40} name="mobile" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={styles.input}>
              <InputItem
                pattern="[0-9]*"
                type="number"
                placeholder={I18n.t('placeHolder_mobile')}
                placeholder="Mobile"
                {...getFieldProps('mobile', {
                  initialValue: get(user, 'mobile', undefined),
                  rules: [
                    {
                      required: true,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_required',
                        'placeHolder_mobile'
                      ),
                    },
                    {
                      WhiteSpace: false,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_whiteSpace',
                        'placeHolder_mobile'
                      ),
                    },
                    {
                      min: 9,
                      message: I18n.t('validate_len_mobile'),
                    },
                  ],
                  getValueFromEvent: v => (v && v.length ? v.replace(/\s/g, '') : undefined),
                })}
                clear
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>
      </View>
    );
  }
}
