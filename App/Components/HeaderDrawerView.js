import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import request from '../Lib/request';
import Loading from './Loading';
import get from 'lodash/get';
import { Flex } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import styles from './Styles/HeaderDrawerViewStyle';

export default class HeaderDrawerView extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  state = {
    user: [],
    errorArr: [],
    isFetching: false,
  };

  componentDidMount() {
    this.getProfile();
  }

  getProfile = () => {
    this.setState({ isFetching: true });
    setTimeout(() => {
      request.get('/user').then(t => {
        const errorArr = get(t.data, 'messages', null);
        if (t.ok) {
          // check error code
          if (get(t.data, 'error_code', 0) > 0) {
            this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
          }

          this.setState({
            user: get(t.data, 'data', []),
          });
        } else {
          this.setState({ errorArr });
        }
        setTimeout(() => {
          this.setState({ isFetching: false });
        }, 2000);
      });
    }, 1000);
  };

  render() {
    const { isFetching, user } = this.state;
    return (
      <View style={styles.container}>
        <Loading loading={isFetching}>
          <Flex justify="between">
            <Text style={styles.textStyle}>您好, {get(user, 'username', 'Guest')}</Text>
            <View
              style={{
                textAlign: 'right',
                marginRight: 15,
              }}
            >
              <TouchableOpacity onPress={this.props.onClose}>
                <Icon name="close" size={24} color="#818181" />
              </TouchableOpacity>
            </View>
          </Flex>
        </Loading>
      </View>
    );
  }
}
