import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import { Picker } from '@ant-design/react-native';
import get from 'lodash/get';
import I18n from '../I18n';

import styles from './Styles/SingleSelectableViewStyle';

export default class SingleSelectableView extends Component {
  // // Prop type warnings
  static propTypes = {
    loading: PropTypes.bool,
    pickerStyle: PropTypes.object,
    containerStyle: PropTypes.object,
  };

  // Defaults for props
  static defaultProps = {
    loading: false,
    pickerStyle: {},
    containerStyle: {},
  };
  getLabel = key => {
    const { data } = this.props;
    if (data && Array.isArray(data)) {
      if (key) {
        const item = data.filter(t => key.includes(t.value));
        return get(item, '[0].label', I18n.t('label_choose'));
      }
    }

    return get(data, '[0].label', I18n.t('label_choose'));
  };

  render() {
    const { form, data, loading, pickerStyle, containerStyle } = this.props;
    return (
      <View style={[styles.container, containerStyle]}>
        <View style={[styles.pickerWraper, pickerStyle]}>
          <Picker
            {...form}
            okText={I18n.t('label_ok')}
            dismissText={I18n.t('label_cancel')}
            data={data}
            cols={1}
          >
            <Text>
              {loading && data && data.length === 0
                ? I18n.t('label_loading')
                : this.getLabel(form.value)}
            </Text>
          </Picker>
        </View>
      </View>
    );
  }
}
