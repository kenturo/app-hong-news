import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image } from 'react-native';
import styles from './Styles/HeaderTitleMainStyle';
import { Images } from '../Themes';
import Icon from 'react-native-vector-icons/FontAwesome';
import HelperFunc from '../Lib/helper';

export default class HeaderTitleMain extends Component {
  // // Prop type warnings
  static propTypes = {
    hiddenDrawer: PropTypes.bool,
  };
  //
  // // Defaults for props
  static defaultProps = {
    hiddenDrawer: false,
  };

  render() {
    const { navigation, DrawerActions, hiddenDrawer } = this.props;

    return (
      <View style={styles.container}>
        {!hiddenDrawer ? (
          <TouchableOpacity
            onPress={() => {
              navigation.dispatch(DrawerActions.toggleDrawer());
            }}
          >
            <Icon style={{ marginLeft: 10 }} name="navicon" size={30} color="#fff" />
          </TouchableOpacity>
        ) : (
          <View />
        )}
        <TouchableOpacity onPress={() => navigation.navigate('HomeTab')}>
          <Image source={Images.logo} style={[styles.img]} resizeMode="center" />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => HelperFunc.loadInBrowser('https://lc.chat/now/10753432/')}>
          <Icon style={{ marginRight: 10 }} name="commenting-o" size={30} color="#fff" />
        </TouchableOpacity>
      </View>
    );
  }
}
