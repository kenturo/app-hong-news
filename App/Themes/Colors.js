const MainTabBottom = {
  activeBackgroundColor: "#ed2939",
  activeTintColor: "#fff",
  inactiveBackgroundColor: "#fff",
  inactiveTintColor: "#828282"
};

const MainTopHeader = {
  backgroundColor: "#ed2939"
};

export default {
  background: "#1F0808",
  clear: "rgba(0,0,0,0)",
  facebook: "#3b5998",
  transparent: "rgba(0,0,0,0)",
  silver: "#F7F7F7",
  steel: "#CCCCCC",
  error: "rgba(200, 0, 0, 0.8)",
  ricePaper: "rgba(255,255,255, 0.75)",
  frost: "#D8D8D8",
  cloud: "rgba(200,200,200, 0.35)",
  windowTint: "rgba(0, 0, 0, 0.4)",
  panther: "#161616",
  charcoal: "#595959",
  coal: "#2d2d2d",
  bloodOrange: "#fb5f26",
  snow: "white",
  ember: "rgba(164, 0, 48, 0.5)",
  fire: "#e73536",
  drawer: "rgba(30, 30, 29, 0.95)",
  eggplant: "#251a34",
  border: "#ed2939",
  banner: "#5F3E63",
  text: "#E0D7E5",
  linkColor: "#007bff",
  headerTitleColor: "#ed2939",
  borderColor: "#ed2939",
  textColor: "#ed2939",
  MainTabBottom,
  MainTopHeader
};
