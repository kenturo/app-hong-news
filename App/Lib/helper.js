import React from 'react';
import { Linking } from 'react-native';
import { Toast } from '@ant-design/react-native';
import * as Sentry from '@sentry/react-native';

import ErrorBarInline from '../Components/ErrorBarInline';
import I18n from '../I18n';

/**
 *
 * @param {Array} err
 */
const getErrorFormForShow = err => {
  if (err) {
    if (typeof err === 'string') {
      return <ErrorBarInline value={err} />;
    }

    if (Array.isArray(err)) {
      return err.map(t => <ErrorBarInline key={t} value={err[t] ? err[t].toString() : null} />);
    }

    return Object.keys(err).map(t => (
      <ErrorBarInline key={t} value={err[t] ? err[t].toString() : null} />
    ));
  }
  return null;
};

const getMesgPlural = (keyPlural, keyField) => I18n.t(keyPlural, { value: I18n.t(keyField) });

const regexValidEmail = email => {
  const REGEX = /^[a-zA-Z0-9@.-_+]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/gim;
  return email.match(REGEX) !== null;
};

const validateEmail = (rule, value, callback) => {
  if (value && regexValidEmail(value)) {
    callback();
  } else {
    callback(new Error(I18n.t('validate_format_email')));
  }
};

validateUrl = url => {
  const REGEX = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm;
  if (url && url.length) {
    const isMatch = url.match(REGEX);
    if (isMatch) {
      return true;
    } else {
      Sentry.captureMessage(
        JSON.stringify({
          title: 'helper/validateUrl: invalid url',
          url,
        }),
        'debug'
      );
      return false;
    }
  }
  return null;
};

const loadInBrowser = url => {
  if (url) {
    if (validateUrl(url)) {
      Linking.openURL(url).catch(err => Toast.fail("Couldn't load page", err));
    } else {
      Toast.fail("Couldn't load url play game", 1);
    }
  } else {
    Toast.fail('Not found game link', 1);
  }
};

export default {
  getErrorFormForShow,
  getMesgPlural,
  validateEmail,
  regexValidEmail,
  loadInBrowser,
};
