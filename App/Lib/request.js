// a library to wrap and simplify api calls
import apisauce from 'apisauce';
import { AsyncStorage } from 'react-native';

import AppConfig from '../Config/AppConfig';
import get from 'lodash/get';

const baseURL = `${AppConfig.HOSTNAME_ENDPOINT}/api/v1`;

const api = apisauce.create({
  // base URL is read from the "constructor"
  baseURL,
  // here are some default headers
  headers: {
    'Cache-Control': 'no-cache',
  },
  // 10 second timeout...
  timeout: 10000,
});

api.addAsyncRequestTransform(request => async () => {
  const isHasToken = get(request, 'headers.Authorization', null);

  if (!isHasToken) {
    const token = await AsyncStorage.getItem('persist:primary').then(data => {
      if (data && Object.keys(data).length > 0) {
        const parseJSON = JSON.parse(data);
        const objLogin = JSON.parse(get(parseJSON, 'login', null));
        return get(objLogin, 'token', null);
      }
      return null;
    });
    request.headers['Authorization'] = `Bearer ${token}`;
  }
});

export default api;
