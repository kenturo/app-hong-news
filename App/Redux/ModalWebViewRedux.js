import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  modalWebViewRequest: ["data"],
  modalWebViewSuccess: ["data"],
  modalWebViewFailure: null
});

export const ModalWebViewTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
});

/* ------------- Selectors ------------- */

export const ModalWebViewSelectors = {
  getData: state => state.data
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null });

// successful api lookup
export const success = (state, action) => {
  const { data } = action;
  return state.merge({ fetching: false, error: null, data });
};

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.MODAL_WEB_VIEW_REQUEST]: request,
  [Types.MODAL_WEB_VIEW_SUCCESS]: success,
  [Types.MODAL_WEB_VIEW_FAILURE]: failure
});
