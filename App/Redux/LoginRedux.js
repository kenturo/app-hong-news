import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  loginRequest: ['payload'],
  loginSuccess: ['token'],
  loginFailure: ['error'],

  setToken: ['token'],
  logout: ['token'],
  reset: [],
});

export const LoginTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  token: null,
  fetching: null,
  payload: null,
  error: null,
});

/* ------------- Selectors ------------- */

export const LoginSelectors = {
  getData: state => state.data,
  getError: state => state.error,
  isFetching: state => state.fetching,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => state.merge({ fetching: true, data, payload: null });

// successful api lookup
export const success = (state, { token }) => {
  return state.merge({ fetching: false, error: null, token });
};

// Something went wrong somewhere.
export const failure = (state, action) => {
  const { error } = action;
  return state.merge({ fetching: false, error });
};

export const reset = state => {
  return state.merge({ fetching: false, error: null });
};
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: request,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.SET_TOKEN]: success,
  [Types.LOGOUT]: success,
  [Types.RESET]: reset,
});
