import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  walletRequest: ['data'],
  walletSuccess: ['data'],
  walletFailure: ['error'],
});

export const WalletTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: [],
  fetching: false,
  error: null,
});

/* ------------- Selectors ------------- */

export const WalletSelectors = {
  getData: state => state.data,
  getError: state => state.error,
  isFetching: state => state.fetching,
};

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data = [] }) => state.merge({ fetching: true, data });

// successful api lookup
export const success = (state, { data = [] }) => {
  return state.merge({ fetching: false, error: null, data });
};

// Something went wrong somewhere.
export const failure = (state, action) => {
  const { error } = action;
  return state.merge({ fetching: false, error });
};
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.WALLET_REQUEST]: request,
  [Types.WALLET_SUCCESS]: success,
  [Types.WALLET_FAILURE]: failure,
});
