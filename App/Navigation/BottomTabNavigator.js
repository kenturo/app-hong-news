import React from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, Image } from 'react-native';
import { createSwitchNavigator } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Colors } from '../Themes';

import HomeScreen from '../Containers/HomeScreen';
import CategoriesDetailListScreen from '../Containers/CategoriesDetailListScreen';
import RegisterScreen from '../Containers/RegisterScreen';
import LoginScreen from '../Containers/LoginScreen';
import WalletScreen from '../Containers/WalletScreen';
import WithdrawScreen from '../Containers/WithdrawScreen';
import TranferMoneyScreen from '../Containers/TranferMoneyScreen';
import DepositScreen from '../Containers/DepositScreen';
import { Images } from '../Themes';

import Icon from 'react-native-vector-icons/FontAwesome';
import IconIon from 'react-native-vector-icons/Ionicons';
import I18n from '../I18n';

const TabBarComponent = props => {
  const { style, onPress, children, route, token } = props;
  const params = route.params;
  const isHasToken = token !== null;

  if (params && isHasToken !== params.isLogin) {
    return null;
  }
  return (
    <View
      style={[
        ...style,
        {
          alignItems: 'center',
        },
      ]}
    >
      <TouchableOpacity onPress={onPress}>{children}</TouchableOpacity>
    </View>
  );
};

const mapStateToProps = state => ({
  token: state.login.token,
});

const TabBarButtonComponent = connect(mapStateToProps)(TabBarComponent);

const HomeStack = createSwitchNavigator(
  {
    Home: {
      screen: HomeScreen,
    },

    CategoriesDetailListScreen: {
      screen: CategoriesDetailListScreen,
    },
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      header: null,
    },
  }
);

const BaseRoute = {
  HomeTab: {
    screen: HomeStack,
    navigationOptions: {
      tabBarLabel: I18n.t('tabBar_home'),
      tabBarIcon: ({ tintColor }) => <Icon name="home" size={20} color={tintColor} />,
    },
  },
  RegisterTab: {
    screen: RegisterScreen,
    params: {
      isLogin: false,
    },
    navigationOptions: {
      title: I18n.t('tabBar_register'),
      tabBarLabel: I18n.t('tabBar_register'),
      tabBarIcon: ({ tintColor }) => <Icon name="user-plus" size={20} color={tintColor} />,
    },
  },
  LoginTab: {
    screen: LoginScreen,
    params: {
      isLogin: false,
    },
    navigationOptions: {
      title: I18n.t('tabBar_login'),
      tabBarLabel: I18n.t('tabBar_login'),
      tabBarIcon: ({ tintColor }) => <Icon name="sign-in" size={20} color={tintColor} />,
    },
  },
  WalletTab: {
    screen: WalletScreen,
    params: {
      isLogin: true,
    },
    navigationOptions: {
      title: I18n.t('tabBar_wallet'),
      tabBarLabel: I18n.t('tabBar_wallet'),
      tabBarIcon: ({ tintColor }) => <IconIon name="ios-wallet" size={20} color={tintColor} />,
    },
  },
  WithdrawTab: {
    screen: WithdrawScreen,
    params: {
      isLogin: true,
    },
    navigationOptions: {
      title: I18n.t('tabBar_withdraw'),
      tabBarLabel: I18n.t('tabBar_withdraw'),
      tabBarIcon: ({ tintColor }) => (
        <Image
          style={{
            width: 20,
            height: 20,
            tintColor: tintColor,
          }}
          source={Images.icoWithDraw}
        />
      ),
    },
  },
  TranferMoneyTab: {
    screen: TranferMoneyScreen,
    params: {
      isLogin: true,
    },
    navigationOptions: {
      title: I18n.t('tabBar_transfer'),
      tabBarLabel: I18n.t('tabBar_transfer'),
      tabBarIcon: ({ tintColor }) => (
        <Image
          style={{
            width: 20,
            height: 20,
            tintColor: tintColor,
          }}
          source={Images.icoTransfer}
        />
      ),
    },
  },
  DepositTab: {
    screen: DepositScreen,
    params: {
      isLogin: true,
    },
    navigationOptions: {
      title: I18n.t('tabBar_deposit'),
      tabBarLabel: I18n.t('tabBar_deposit'),
      tabBarIcon: ({ tintColor }) => (
        <Image
          style={{
            width: 20,
            height: 20,
            tintColor: tintColor,
          }}
          source={Images.icoDeposit}
        />
      ),
    },
  },
};

export default createBottomTabNavigator(BaseRoute, {
  initialRouteName: 'HomeTab',
  tabBarOptions: Colors.MainTabBottom,
  defaultNavigationOptions: () => ({
    tabBarButtonComponent: p => <TabBarButtonComponent {...p} />,
  }),
});
