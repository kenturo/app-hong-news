import React from 'react';
import { ScrollView, SafeAreaView, View, Text, TouchableOpacity, Image } from 'react-native';
import { Flex } from '@ant-design/react-native';
import { DrawerActions } from 'react-navigation-drawer';
import { connect } from 'react-redux';
import get from 'lodash/get';
import { Colors, Images } from '../Themes';
import LoginActions from '../Redux/LoginRedux';
import HeaderDrawerView from '../Components/HeaderDrawerView';

import Icon from 'react-native-vector-icons/FontAwesome';
import IconIon from 'react-native-vector-icons/Ionicons';
import I18n from '../I18n';

const mapDispatchToProps = dispatch => {
  return {
    onLogout: () => {
      dispatch(LoginActions.logout(null));
    },
  };
};

const mapStateToProps = state => {
  return { token: get(state.login, 'token', null) };
};

const routerAfterLogin = [
  // {
  //   title: 'Home',
  //   tabName: 'HomeTab',
  //   icon: <Icon name="home" size={20} color={Colors.textColor} />,
  // },
  {
    title: '钱包',
    tabName: 'WalletTab',
    icon: <IconIon name="ios-wallet" size={26} color={Colors.textColor} />,
  },
  {
    title: '提款',
    tabName: 'WithdrawTab',
    icon: (
      <Image
        style={{
          width: 24,
          height: 24,
          tintColor: Colors.textColor,
        }}
        source={Images.icoWithDraw}
      />
    ),
  },
  {
    title: '转账',
    tabName: 'TranferMoneyTab',
    icon: (
      <Image
        style={{
          width: 24,
          height: 24,
          tintColor: Colors.textColor,
        }}
        source={Images.icoTransfer}
      />
    ),
  },
  {
    title: '存款',
    tabName: 'DepositTab',
    icon: (
      <Image
        style={{
          width: 24,
          height: 24,
          tintColor: Colors.textColor,
        }}
        source={Images.icoDeposit}
      />
    ),
  },
  {
    title: '账号信息',
    tabName: 'ProfileScreen',
    icon: <Icon name="user" size={26} color={Colors.textColor} />,
  },
  {
    title: '存款记录',
    tabName: 'HistoryDepositScreen',
    icon: (
      <Image
        style={{
          width: 24,
          height: 24,
          tintColor: Colors.textColor,
        }}
        source={Images.icoHistoryDepoWithdraw}
      />
    ),
  },
  {
    title: '提款记录',
    tabName: 'HistoryWithdrawScreen',
    icon: (
      <Image
        style={{
          width: 24,
          height: 24,
          tintColor: Colors.textColor,
        }}
        source={Images.icoHistoryDepoWithdraw}
      />
    ),
  },
  {
    title: '转账记录',
    tabName: 'HistoryTransferScreen',
    icon: (
      <Image
        style={{
          width: 24,
          height: 24,
          tintColor: Colors.textColor,
        }}
        source={Images.icoHistoryTransfer}
      />
    ),
  },
  {
    title: '投注历史',
    tabName: 'HistoryBettingScreen',
    icon: (
      <Image
        style={{
          width: 24,
          height: 24,
          tintColor: Colors.textColor,
        }}
        source={Images.icoHistoryBetting}
      />
    ),
  },
  {
    title: '优惠',
    tabName: 'Promotion',
    icon: <Icon name="gift" size={26} color={Colors.textColor} />,
  },
];

const routerBeforeLogin = [
  {
    title: I18n.t('tabBar_home'),
    tabName: 'HomeTab',
    icon: <Icon name="home" size={26} color={Colors.textColor} />,
  },
  {
    title: I18n.t('tabBar_register'),
    tabName: 'RegisterTab',
    icon: <Icon name="user-plus" size={24} color={Colors.textColor} />,
  },
  {
    title: I18n.t('tabBar_login'),
    tabName: 'LoginTab',
    icon: <Icon name="sign-in" size={26} color={Colors.textColor} />,
  },
];

const RenderMenu = ({ navigation, router }) =>
  router.map((t, k) => (
    <TouchableOpacity
      key={k}
      onPress={() => {
        navigation.dispatch(DrawerActions.toggleDrawer());
        navigation.navigate({
          routeName: t.tabName,
        });
      }}
    >
      <Flex direction="row" style={styleSheet.wrapFlexItem}>
        <View style={styleSheet.txtMenu}>{t.icon}</View>
        <View>
          <Text style={[styleSheet.txtMenu, styleSheet.txtSize]}>{t.title}</Text>
        </View>
      </Flex>
    </TouchableOpacity>
  ));

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(props => {
  const { onLogout, navigation, token } = props;
  return (
    <ScrollView>
      <SafeAreaView
        style={{
          flex: 1,
        }}
        forceInset={{ top: 'always', horizontal: 'never' }}
      >
        {token ? (
          <View>
            <HeaderDrawerView
              onClose={() => {
                navigation.dispatch(DrawerActions.toggleDrawer());
              }}
            />

            <RenderMenu navigation={navigation} router={routerAfterLogin} />
            <TouchableOpacity
              onPress={() => {
                onLogout();
                navigation.dispatch(DrawerActions.toggleDrawer());
                navigation.navigate({
                  routeName: 'HomeTab',
                });
                navigation.navigate({
                  routeName: 'Home',
                });
              }}
            >
              <Flex direction="row">
                <View style={styleSheet.txtMenu}>
                  <Icon name="sign-out" size={26} color={Colors.textColor} />
                </View>
                <View>
                  <Text style={[styleSheet.txtMenu, styleSheet.txtSize]}>登出</Text>
                </View>
              </Flex>
            </TouchableOpacity>
          </View>
        ) : (
          <RenderMenu navigation={navigation} router={routerBeforeLogin} />
        )}
      </SafeAreaView>
    </ScrollView>
  );
});

const styleSheet = {
  wrapFlexItem: {
    borderBottomWidth: 0.3,
    borderColor: '#818181',
  },
  txtMenu: {
    marginLeft: 15,
    marginVertical: 10,
  },
  txtSize: {
    fontSize: 18,
    color: '#818181',
  },
};
