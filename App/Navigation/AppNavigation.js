import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import HomeStack from "./CreateNavigator";
import LaunchScreen from "../Containers/LaunchScreen";

// Manifest of possible screens
const PrimaryNav = createStackNavigator(
  {
    HomeScreen: { screen: HomeStack },
    LaunchScreen: { screen: LaunchScreen }
  },
  {
    // Default config for all screens
    headerMode: "none",
    initialRouteName: "LaunchScreen"
  }
);

// export default props => {
//   console.log("TCL: PrimaryNav -> props", props);

//   return createAppContainer(PrimaryNav);
// };

export default createAppContainer(PrimaryNav);
