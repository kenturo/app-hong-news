import { createDrawerNavigator } from 'react-navigation-drawer';

import DrawerItemTemplate from './DrawerItemTemplate';

import BottomTabNavigator from './BottomTabNavigator';

// import WalletScreen from "../Containers/WalletScreen";
// import WithdrawScreen from "../Containers/WithdrawScreen";
// import TranferMoneyScreen from "../Containers/TranferMoneyScreen";
// import DepositScreen from "../Containers/DepositScreen";

export default createDrawerNavigator(
  {
    Home: BottomTabNavigator,
  },
  {
    drawerWidth: '100%',
    contentComponent: DrawerItemTemplate,
  }
);
