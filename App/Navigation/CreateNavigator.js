import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { DrawerActions } from 'react-navigation-drawer';
import HeaderTitleMain from '../Components/HeaderTitleMain';
import DrawerNavigator from './DrawerNavigator';
import CategoriesDetailListScreen from '../Containers/CategoriesDetailListScreen';
import ResetPasswordScreen from '../Containers/ResetPasswordScreen';
import ProfileScreen from '../Containers/ProfileScreen';
import HistoryDepositScreen from '../Containers/HistoryDepositScreen';
import HistoryBettingScreen from '../Containers/HistoryBettingScreen';
import HistoryWithdrawScreen from '../Containers/HistoryWithdrawScreen';
import HistoryTransferScreen from '../Containers/HistoryTransferScreen';
import PromotionScreen from '../Containers/PromotionScreen';

import { Colors } from '../Themes';

// const DrawersNav = DrawerNavigator(false);
const navigationOptions = ({ navigation }) => ({
  headerTitle: (
    <HeaderTitleMain hiddenDrawer={true} navigation={navigation} DrawerActions={DrawerActions} />
  ),
  headerTintColor: '#fff',
});

const MainStackNavigate = createStackNavigator(
  {
    HomeScreen: { screen: DrawerNavigator },
    ResetPassword: {
      screen: ResetPasswordScreen,
      navigationOptions,
    },
    Promotion: {
      screen: PromotionScreen,
      navigationOptions,
    },

    ProfileScreen: {
      screen: ProfileScreen,
      navigationOptions,
    },
    HistoryDepositScreen: {
      screen: HistoryDepositScreen,
      navigationOptions,
    },
    HistoryWithdrawScreen: {
      screen: HistoryWithdrawScreen,
      navigationOptions,
    },
    HistoryTransferScreen: {
      screen: HistoryTransferScreen,
      navigationOptions,
    },
    HistoryBettingScreen: {
      screen: HistoryBettingScreen,
      navigationOptions,
    },
    CategoriesDetailListScreen: {
      screen: CategoriesDetailListScreen,
      navigationOptions,
    },
  },
  {
    // initialRouteName: "ProfileScreen",
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: <HeaderTitleMain navigation={navigation} DrawerActions={DrawerActions} />,
      headerStyle: Colors.MainTopHeader,
    }),
  }
);
export default MainStackNavigate;
