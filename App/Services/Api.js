// a library to wrap and simplify api calls
import apisauce from 'apisauce';
import request from '../Lib/request';

const checkResponseStatus = res => {
  try {
    if (res && res.ok) {
      const { data, error_code } = res.data;

      if (error_code === 0) {
        return {
          ok: res.ok,
          data,
        };
      }
    } else {
      return {
        ok: res.ok,
        data: res.data[0].message,
      };
    }
  } catch (error) {
    return {
      ok: false,
      data: error,
    };
  }
};

// our "constructor"
const create = () => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //

  // ------
  // STEP 2
  // ------
  //
  // Define some functions that call the api.  The goal is to provide
  // a thin wrapper of the api layer providing nicer feeling functions
  // rather than "get", "post" and friends.
  //
  // I generally don't like wrapping the output at this level because
  // sometimes specific actions need to be take on `403` or `401`, etc.
  //
  // Since we can't hide from that, we embrace it by getting out of the
  // way at this level.
  //
  const getlogin = data => request.post('/login', data).then(checkResponseStatus);

  const getWalletList = () => request.get('/wallets').then(checkResponseStatus);

  const getRoot = () => request.get('');
  const getRate = () => request.get('rate_limit');
  const getUser = username => request.get('search/users', { q: username });

  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return {
    // a list of the API functions from step 2
    getRoot,
    getRate,
    getUser,
    getlogin,
    getWalletList,
  };
};

// let's return back our create method as the default.
export default {
  create,
};
