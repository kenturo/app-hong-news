import React, { Component } from 'react';
import { ScrollView, Text, View, Dimensions } from 'react-native';
import { WingBlank, WhiteSpace, Button, Flex } from '@ant-design/react-native';
import { createForm } from 'rc-form';
import qs from 'querystringify';
import moment from 'moment';
import request from '../Lib/request';
import get from 'lodash/get';
import find from 'lodash/find';

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import TitleScreen from '../Components/TitleScreen';
import TableList from '../Components/TableList';
import SingleSelectableView from '../Components/SingleSelectableView';
// import IconScreen from '../Components/IconScreen';
import SearchHistories from '../Components/SearchHistories';
// import HelperFunc from '../Lib/helper';
// import Loading from '../Components/Loading';
// import WalletActions from '../Redux/WalletRedux';
import I18n from '../I18n';
// Styles
import styles from './Styles/HistoryDepositScreenStyle';

class HistoryBettingScreen extends Component {
  state = {
    data: [],
    isFetching: false,
    errorArr: [],
    paging: {},
    bettingType: [],
    query: {},
    columnsHead: ['赢/输时间', '选择', '投注金额', '赢/输金额', '状态'],
    rowDisplay: [
      {
        key: 'updated_at',
        render: r => (
          <Text
            style={[
              styles.txtStyle,
              {
                lineHeight: 20,
              },
            ]}
          >
            {moment(r).format('hh:mm YYYY/MM/DD')}
          </Text>
        ),
      },
      {
        key: 'json',
        render: r => {
          let parseJson = JSON.parse(r);
          let txt = get(parseJson, 'description.game_type.name', '');

          return (
            <Text style={styles.txtStyle}>
              {txt.split('_').length > 1 ? txt.split('_')[1] : txt}
            </Text>
          );
        },
      },
      {
        key: 'bet_amount',
        render: r => <Text style={styles.txtStyle}>{`¥${r}`}</Text>,
      },
      {
        key: 'net_amount',
        render: r => <Text style={styles.txtStyle}>{`¥${r}`}</Text>,
      },
      {
        key: 'net_amount',
        render: r => {
          let text = null;
          let styleTxt = 'txtStatusBal';

          if (Number(r) > 0) {
            text = '赢'; //'WON';
            styleTxt = 'txtStatusDone';
          } else if (Number(r) < 0) {
            text = '输'; //'LOSE';
            styleTxt = 'txtStatusFail';
          } else {
            text = '和局'; //'DRAW';
            styleTxt = 'txtStatusBal';
          }

          return (
            <View style={styles.txtStyle}>
              <Text style={[styles.txtStatus, styles[styleTxt]]}>{text}</Text>
            </View>
          );
        },
      },
    ],
    widthArr: new Array(5).fill(Dimensions.get('window').width / 5),
    styleWrapperRow: {},
  };

  // apply for bettingType not equal type sportbook
  rowDisplayForBetting = [
    {
      key: 'updated_at',
      render: r => (
        <Text
          style={[
            styles.txtStyle,
            {
              lineHeight: 20,
            },
          ]}
        >
          {moment(r).format('hh:mm YYYY/MM/DD')}
        </Text>
      ),
    },
    {
      key: 'json',
      render: r => {
        let parseJson = JSON.parse(r);
        let txt = get(parseJson, 'description.game_type.name', '');

        return (
          <Text style={styles.txtStyle}>{txt.split('_').length > 1 ? txt.split('_')[1] : txt}</Text>
        );
      },
    },
    {
      key: 'bet_amount',
      render: r => <Text style={styles.txtStyle}>{`¥${r}`}</Text>,
    },
    {
      key: 'net_amount',
      render: r => <Text style={styles.txtStyle}>{`¥${r}`}</Text>,
    },
    {
      key: 'net_amount',
      render: r => {
        let text = null;
        let styleTxt = 'txtStatusBal';

        if (Number(r) > 0) {
          text = '赢'; //'WON';
          styleTxt = 'txtStatusDone';
        } else if (Number(r) < 0) {
          text = '输'; //'LOSE';
          styleTxt = 'txtStatusFail';
        } else {
          text = '和局'; //'DRAW';
          styleTxt = 'txtStatusBal';
        }

        return (
          <View style={styles.txtStyle}>
            <Text style={[styles.txtStatus, styles[styleTxt]]}>{text}</Text>
          </View>
        );
      },
    },
  ];

  rowDisplaySportBook = [
    {
      key: 'created_at',
      render: r => (
        <Flex justify="end" style={[styles.txtStyle]}>
          <Text
            style={[
              styles.txtStyle,
              {
                lineHeight: 20,
              },
            ]}
          >
            {moment(r).format('hh:mm YYYY/MM/DD')}
          </Text>
        </Flex>
      ),
    },
    {
      key: 'json',
      render: (r, record) => {
        let parseJson = JSON.parse(r);
        const away = get(parseJson, 'description.away.names', null);
        const awayName = find(away, { lang: 'ch' });

        const home = get(parseJson, 'description.home.names', null);
        const homeName = find(home, { lang: 'ch' });

        const league = get(parseJson, 'description.league.names', null);
        const leagueName = find(league, { lang: 'ch' });

        const sportName = get(parseJson, 'description.sport.name', null);
        const betTypeName = get(parseJson, 'description.bet_type.name', null);

        return (
          <Flex direction="column" wrap="wrap" align="baseline">
            {betTypeName ? <Text style={styles.textDes}>{betTypeName}</Text> : null}
            {awayName ? (
              <Text style={styles.textDes}>{`${get(awayName, 'name', null)} ${get(
                parseJson,
                'response.odds',
                null
              )}`}</Text>
            ) : null}
            {homeName ? (
              <Text style={styles.textDes}>{`${get(homeName, 'name', null)} ${get(
                parseJson,
                'description.match.[0].home_score',
                null
              )} : ${get(parseJson, 'description.match.[0].away_score', null)} ${get(
                awayName,
                'name',
                null
              )}`}</Text>
            ) : null}
            {leagueName ? (
              <Text style={styles.textDes}>{`${get(leagueName, 'name', null)}`}</Text>
            ) : null}
            {sportName ? <Text style={styles.textDes}>{sportName}</Text> : null}
            <Text style={styles.textDes}>-----------</Text>
            <Text style={styles.textDes}>{`投注额: ¥${get(record, 'stake', null)}`}</Text>
            <Text style={styles.textDes}>{`赢/输: ¥${get(record, 'winlost_amount', null)}`}</Text>
          </Flex>
        );
      },
    },
    {
      key: 'ticket_status',
      render: status => {
        let text = null;
        let styleTxt = 'txtStatusBal';

        switch (status.toUpperCase()) {
          case 'Half WON':
          case 'WON':
            text = '赢'; //'WON';
            styleTxt = 'txtStatusDone';
            break;

          case 'DRAW':
            text = '和局'; //'DRAW';
            styleTxt = 'txtStatusBal';
            break;
          case 'LOSE':
          case 'Half LOSE':
            text = '输'; //'LOSE';
            styleTxt = 'txtStatusFail';
            break;

          default:
            text = status; //'DRAW';
            styleTxt = 'txtStatusBal';
            break;
        }

        return (
          <Flex justify="end" style={[styles.txtStyle, { textAlign: 'right' }]}>
            <Text style={[styles.txtStatus, styles[styleTxt]]}>{text}</Text>
          </Flex>
        );
      },
    },
  ];

  componentDidMount() {
    this.getBettingType();
  }

  onChangeColumnByBetting = bettingType => {
    let rowDisplay = this.rowDisplayForBetting;
    let columnsHead = ['赢/输时间', '选择', '投注金额', '赢/输金额', '状态'];
    const wCell = Dimensions.get('window').width / columnsHead.length;
    let widthArr = new Array(columnsHead.length).fill(wCell);
    let styleWrapperRow = null;

    if (bettingType === 'sportbook') {
      rowDisplay = this.rowDisplaySportBook;
      columnsHead = ['时间', '选项', '状态'];
      widthArr = [50, Dimensions.get('window').width - 100, 50];
      styleWrapperRow = { height: 150 };
    }

    return { rowDisplay, columnsHead, widthArr, styleWrapperRow };
  };

  getBettingType = () => {
    this.setState({ isFetching: true });
    request.get(`/history/betting_type`).then(t => {
      const errorArr = get(t.data, 'messages', null);
      if (t.ok) {
        // check error code
        if (get(t.data, 'error_code', 0) > 0) {
          this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
        } else {
          const data = get(t.data, 'data', []);
          this.setState({
            bettingType: data.map(t => ({
              label: t.text,
              value: t.value,
            })),
          });
        }
      } else {
        this.setState({ errorArr });
      }
      this.setState({ isFetching: false });
    });
  };

  getBetting = nextquery => {
    this.setState({ isFetching: true });
    const mergeQ = Object.assign({}, this.state.query, nextquery);
    // const rowColumTable = this.onChangeColumnByBetting(mergeQ.game.toString());
    this.setState({ query: mergeQ });

    const qr = qs.stringify(mergeQ);
    request.get(`/history/betting?${qr}`).then(t => {
      const errorArr = get(t.data, 'messages', null);
      if (t.ok) {
        // check error code
        if (get(t.data, 'error_code', 0) > 0) {
          this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
        }

        const { data, ...rest } = get(t.data, 'data', {});
        this.setState({
          data,
          paging: rest,
        });
      } else {
        this.setState({ errorArr });
      }
      this.setState({ isFetching: false });
    });
  };

  onSubmit = () => {
    const { validateFields } = this.props.form;
    validateFields((err, payload) => {
      const rowColumTable = this.onChangeColumnByBetting(payload.game.toString());
      this.setState({ ...rowColumTable });
      this.getBetting({
        ...payload.time,
        game: payload.game,
      });
    });
  };

  render() {
    const {
      isFetching,
      data,
      paging,
      bettingType,
      columnsHead,
      rowDisplay,
      styleWrapperRow,
      widthArr,
    } = this.state;
    const { form } = this.props;
    const { getFieldProps, getFieldDecorator, setFieldsValue, getFieldValue } = form;
    const wCell = Dimensions.get('window').width / columnsHead.length;

    getFieldDecorator('time');

    return (
      <ScrollView style={styles.container}>
        <TitleScreen value={I18n.t('label_history_bet')} />
        <WhiteSpace size="xl" />
        <WingBlank>
          <SearchHistories onChange={v => setFieldsValue({ time: v })}>
            <SingleSelectableView
              data={bettingType}
              form={getFieldProps('game', {
                initialValue: 'livecasino',
              })}
              loading={isFetching}
              containerStyle={{
                paddingHorizontal: 0,
                marginHorizontal: 0,
              }}
              pickerStyle={{
                height: 37,
                flex: 0,
                borderWidth: 0,
                paddingLeft: 0,
              }}
            />
          </SearchHistories>
          <WhiteSpace />
          <Flex justify="end">
            <Button
              type="warning"
              onPress={this.onSubmit}
              loading={isFetching}
              style={{
                width: 100,
                height: 35,
              }}
            >
              {I18n.t('btn_search')}
            </Button>
          </Flex>
        </WingBlank>
        <WhiteSpace size="xl" />
        <WingBlank>
          <TableList
            styleWrapperRow={styleWrapperRow}
            width={wCell * columnsHead.length}
            columnsHead={columnsHead}
            columnData={rowDisplay}
            widthArr={widthArr}
            heightArr={new Array(columnsHead.length).fill(80)}
            isFetching={isFetching}
            data={data}
            paging={paging}
            onPagingChange={this.getBetting}
          />
        </WingBlank>
      </ScrollView>
    );
  }
}

export default createForm()(HistoryBettingScreen);
