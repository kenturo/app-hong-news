import React, { Component } from 'react';
import { ScrollView, KeyboardAvoidingView } from 'react-native';
import { createForm } from 'rc-form';
import {
  Button,
  Flex,
  WhiteSpace,
  WingBlank,
  InputItem,
  Toast,
  Portal,
} from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import TitleScreen from '../Components/TitleScreen';
import * as Sentry from '@sentry/react-native';

import get from 'lodash/get';
import request from '../Lib/request';
import HelperFunc from '../Lib/helper';
import I18n from '../I18n';

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/WithdrawScreenStyle';

/**
 * bank_name: 中国工商银行
account_name: test
account_number: test
amount: 10
 */

class WithdrawScreen extends Component {
  state = {
    errorArr: [],
    isFetching: false,
    bankInfo: {},
  };

  componentDidMount() {
    this.getBankInfo();
    this.props.navigation.addListener('didFocus', () => {
      this.getBankInfo();
    });
  }

  getBankInfo = () => {
    this.setState({ isFetching: true });
    request.get('/bank_info').then(t => {
      const errorArr = get(t.data, 'messages', null);
      if (t.ok) {
        // check error code
        if (get(t.data, 'error_code', 0) > 0) {
          this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
        }

        this.setState({
          bankInfo: get(t.data, 'data', []),
        });
      } else {
        this.setState({ errorArr });
      }
      this.setState({ isFetching: false });
    });
  };

  onSubmit = () => {
    const { validateFields, getFieldsError } = this.props.form;
    validateFields((err, payload) => {
      if (!err) {
        this.setState({ errorArr: null });

        const fd = new FormData();
        Object.keys(
          Object.assign({}, payload, {
            amount: Number(payload.amount),
          })
        ).forEach(function(key) {
          fd.append(key, payload[key]);
        });

        const toast = Toast.loading(I18n.t('label_loading'), 0);

        try {
          request.post('/withdraw', fd).then(t => {
            const errorArr = get(t.data, 'messages', null);

            if (t.ok) {
              // check error code
              if (get(t.data, 'error_code', 0) > 0) {
                this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
              } else {
                this.setState({
                  errorArr: null,
                });
                // success
                Toast.success(get(t.data, 'message', null), 1);
              }
            } else {
              this.setState({ errorArr });
            }
            Portal.remove(toast);
          });
        } catch (error) {
          Sentry.captureException(new Error(error));
          Portal.remove(toast);
        }
      } else {
        this.setState({ errorArr: getFieldsError() });
      }
    });
  };

  render() {
    const { errorArr, isFetching, bankInfo } = this.state;
    const { form } = this.props;
    const { getFieldProps } = form;

    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior="padding">
          <TitleScreen value={I18n.t('label_withdraw')} />

          <WhiteSpace size="xl" />
          {HelperFunc.getErrorFormForShow(errorArr)}

          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="institution" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  placeholder={I18n.t('placeHolder_bankName')}
                  {...getFieldProps('bank_name', {
                    initialValue: get(bankInfo, 'bank_name', undefined),
                    rules: [
                      {
                        required: true,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_required',
                          'placeHolder_bankName'
                        ),
                      },
                      {
                        min: 1,
                        message: I18n.t('validate_len_bankName'),
                      },
                      {
                        WhiteSpace: false,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_whiteSpace',
                          'placeHolder_bankName'
                        ),
                      },
                    ],
                  })}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>

          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="user" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  placeholder={I18n.t('placeHolder_accountName')}
                  {...getFieldProps('account_name', {
                    initialValue: get(bankInfo, 'account_name', undefined),
                    rules: [
                      {
                        required: true,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_required',
                          'placeHolder_accountName'
                        ),
                      },
                      {
                        min: 1,
                        message: I18n.t('validate_len_accountName'),
                      },
                      {
                        WhiteSpace: false,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_whiteSpace',
                          'placeHolder_accountName'
                        ),
                      },
                    ],
                  })}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>

          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="credit-card" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  placeholder={I18n.t('placeHolder_accountNumber')}
                  {...getFieldProps('account_number', {
                    initialValue: get(bankInfo, 'account_number', undefined),
                    rules: [
                      {
                        required: true,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_required',
                          'placeHolder_accountNumber'
                        ),
                      },
                      {
                        min: 1,
                        message: I18n.t('validate_len_accountNumber'),
                      },
                      {
                        WhiteSpace: false,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_whiteSpace',
                          'placeHolder_accountNumber'
                        ),
                      },
                    ],
                  })}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>

          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="cny" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  type="number"
                  style={styles.input}
                  placeholder={I18n.t('plural_field_amount', { value: '10-50,000' })}
                  {...getFieldProps('amount', {
                    rules: [
                      {
                        required: true,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_required',
                          'placeHolder_amount'
                        ),
                      },
                      {
                        WhiteSpace: false,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_whiteSpace',
                          'placeHolder_amount'
                        ),
                      },
                    ],
                    getValueFromEvent: e =>
                      e
                        ? e
                            .replace(/^0/, '')
                            .replace(/[a-zA-Z]\w+/, '')
                            .trim()
                        : e,
                  })}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>

          <WhiteSpace size="xl" />
          <WingBlank>
            <Button type="warning" onPress={this.onSubmit} loading={isFetching}>
              {I18n.t('btn_withdraw')}
            </Button>
          </WingBlank>

          <WhiteSpace size="xl" />
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}
export default createForm()(WithdrawScreen);
