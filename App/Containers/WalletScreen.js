import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, Text, View } from 'react-native';
import { Card, WingBlank, WhiteSpace } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import TitleScreen from '../Components/TitleScreen';
import HelperFunc from '../Lib/helper';
import Loading from '../Components/Loading';
import WalletActions from '../Redux/WalletRedux';
import I18n from '../I18n';
// Styles
import styles from './Styles/WalletScreenStyle';

class WalletScreen extends Component {
  componentDidMount() {
    const { onLoadWalletList, data } = this.props;
    onLoadWalletList();

    this.props.navigation.addListener('didFocus', () => {
      if (data && data.length) {
        onLoadWalletList();
      }
    });
  }

  render() {
    const { data, error, fetching } = this.props;
    return (
      <ScrollView style={styles.container}>
        <TitleScreen value={I18n.t('label_wallet')} />
        <WhiteSpace size="xl" />
        <Loading loading={fetching}>
          {HelperFunc.getErrorFormForShow(error)}
          {data.map(t => (
            <WingBlank key={t.id}>
              <Card style={styles.wrap}>
                <Card.Body>
                  <View style={{ marginLeft: 16, marginVertical: 10 }}>
                    <Text style={styles.txtTitle}>{t.name}</Text>
                    <Text style={styles.txtMoney}>
                      <Icon name="cny" size={20} color={'red'} /> {t.amount}
                    </Text>
                  </View>
                </Card.Body>
              </Card>
              <WhiteSpace size="xl" />
            </WingBlank>
          ))}

          <WhiteSpace size="xl" />
        </Loading>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => state.wallet;

const mapDispatchToProps = dispatch => {
  return {
    onLoadWalletList: () => {
      dispatch(WalletActions.walletRequest());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WalletScreen);
