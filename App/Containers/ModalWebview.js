import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { WebView } from "react-native-webview";
import { Modal, Flex } from "@ant-design/react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import ModalWebViewAction from "../Redux/ModalWebViewRedux";
import AppConfig from "../Config/AppConfig";
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from "./Styles/ModalWebviewStyle";

class ModalWebview extends Component {
  state = {
    visible: false
  };

  componentDidUpdate() {
    const { data } = this.props;
    if (!this.state.visible && data && data.length > 0) {
      this.setState({ visible: true });
    }
  }

  handleUrl = url => {
    const REGEX = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm;
    if (url && url.length) {
      const isMatch = url.match(REGEX);
      if (isMatch) {
        return isMatch[0];
      } else {
        return `${AppConfig.HOSTNAME_ENDPOINT}${url}`;
      }
    }
    return null;
  };

  onClose = () => {
    const { onOpenWebView } = this.props;
    onOpenWebView(null);
    this.setState({ visible: false });
  };

  render() {
    const { data } = this.props;
    return (
      <Modal
        popup
        visible={this.state.visible}
        animationType="slide-up"
        onClose={() =>
          this.setState({
            visible: false
          })
        }
      >
        <View style={styles.container}>
          <Flex style={styles.bar} justify="around" align="center">
            <Text style={styles.address}>{this.handleUrl(data)}</Text>
            <View style={styles.icoClose}>
              <TouchableOpacity onPress={() => this.onClose()}>
                <Icon name="close" size={20} color="#000" />
              </TouchableOpacity>
            </View>
          </Flex>
          <WebView
            style={{
              flex: 1
            }}
            automaticallyAdjustContentInsets={true}
            scalesPageToFit={true}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            source={{ uri: data }}
          />
        </View>
      </Modal>
    );
  }
}

const mapStateToProps = state => state.modalWebView;

const mapDispatchToProps = dispatch => {
  return {
    onOpenWebView: data => {
      dispatch(ModalWebViewAction.modalWebViewRequest(data));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalWebview);
