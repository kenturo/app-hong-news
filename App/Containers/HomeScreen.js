import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, View } from 'react-native';
import MainBanner from '../Components/MainBanner';
// import QuickMenu from "../Components/QuickMenu";
import CategoriesCardView from '../Components/CategoriesCardView';
import styles from './Styles/LaunchScreenStyles';
import get from 'lodash/get';
import LoginActions from '../Redux/LoginRedux';

class HomeScreen extends Component {
  state = {
    loading: false,
    bannerList: [],
  };

  arrType = ['top-sports', 'top-livecasinos', 'top-slots', 'top-lotto'];

  render() {
    const { token, navigation } = this.props;

    return (
      <ScrollView>
        <View style={styles.mainContainer}>
          <MainBanner />
          {this.arrType.map(t => (
            <CategoriesCardView token={token} navigation={navigation} type={t} key={t} />
          ))}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  token: get(state.login, 'token', null),
});

const mapDispatchToProps = dispatch => {
  return {
    onLogin: payload => {
      dispatch(LoginActions.loginRequest(payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
