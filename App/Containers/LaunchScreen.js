import React, { PureComponent } from 'react';
import { ScrollView, Text, Image, View } from 'react-native';
import { Images } from '../Themes';

// Styles
import styles from './Styles/LaunchScreenStyles';

export default class LaunchScreen extends PureComponent {
  componentDidMount() {
    this.onNavHome();
    this.props.navigation.addListener('didFocus', () => {
      this.onNavHome();
    });
  }

  onNavHome = () => {
    setTimeout(() => {
      this.props.navigation.navigate({
        routeName: 'HomeTab',
      });
    }, 500);
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode="stretch" />
        <View style={styles.centered}>
          <Image source={Images.logo} style={styles.logo} />
        </View>
      </View>
    );
  }
}
