import React, { Component } from 'react';
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import {
  Button,
  Flex,
  WhiteSpace,
  WingBlank,
  InputItem,
  ActivityIndicator,
  Toast,
} from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import HelperFunc from '../Lib/helper';
import ErrorBarInline from '../Components/ErrorBarInline';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import LoginActions from '../Redux/LoginRedux';

import I18n from '../I18n';

// Styles
import styles from './Styles/LoginScreenStyle';

class LoginScreen extends Component {
  state = {
    username: null,
    password: null,
    animating: false,
  };

  componentDidMount() {
    this.props.navigation.addListener('didFocus', () => {
      this.props.onReset();
      this.setState({
        username: null,
        password: null,
      });
    });
  }

  componentDidUpdate() {
    const { fetching, navigation, error } = this.props;
    // console.log(this.props);
    if (fetching !== null && !fetching && this.state.animating) {
      this.setState({ animating: false });

      if (error === null) {
        this.setState({
          username: null,
          password: null,
        });
        navigation.navigate({
          routeName: 'HomeTab',
        });
      }
    }
  }

  componentWillUnmount() {
    this.setState({ animating: false });
  }

  onSubmit = () => {
    const { onLogin } = this.props;
    const { username, password } = this.state;

    if (!username) {
      Toast.fail(HelperFunc.getMesgPlural('plural_field_required', 'placeHolder_username'), 2);
      return;
    }

    if (!password) {
      Toast.fail(HelperFunc.getMesgPlural('plural_field_required', 'placeHolder_password'), 2);
      return;
    }

    if (username && password) {
      this.setState({ animating: true });
      onLogin(this.state);
    }
  };

  render() {
    const { error, navigation } = this.props;
    const { username, password } = this.state;
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior="padding">
          <WhiteSpace size="xl" />
          <Flex direction="column">
            <Flex.Item>
              <Text style={styles.headerTitle} adjustsFontSizeToFit allowFontScaling>
                {I18n.t('label_login')}
              </Text>
            </Flex.Item>
          </Flex>

          <WhiteSpace size="xl" />
          <ErrorBarInline value={error} />

          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="user" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  value={username}
                  onChange={e =>
                    this.setState({
                      username: e && e.length ? e.replace(/\s/g, '') : undefined,
                    })
                  }
                  placeholder={I18n.t('placeHolder_username')}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>

          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="lock" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  value={password}
                  onChange={e => this.setState({ password: e })}
                  placeholder={I18n.t('placeHolder_password')}
                  type="password"
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>

          <WhiteSpace size="xl" />
          <WingBlank>
            <Button type="warning" onPress={() => this.onSubmit()}>
              {I18n.t('btn_login')}
            </Button>
          </WingBlank>

          <WhiteSpace size="xl" />
          <WhiteSpace />
          <Flex direction="column">
            <Flex.Item>
              <Text
                style={[
                  styles.headerTitle,
                  {
                    fontSize: 20,
                  },
                ]}
                adjustsFontSizeToFit
                allowFontScaling
              >
                {I18n.t('label_doyouhave_account')}
              </Text>
            </Flex.Item>
          </Flex>

          <WhiteSpace size="xl" />
          <WingBlank>
            <Button
              style={{
                backgroundColor: 'transparent',
                borderRadius: 50,
              }}
              type="warning"
              onPress={() => {
                navigation.navigate({
                  routeName: 'RegisterTab',
                });
              }}
            >
              <Text
                style={{
                  color: '#db4345',
                }}
              >
                {I18n.t('btn_register')}
              </Text>
            </Button>
          </WingBlank>

          <WhiteSpace size="xl" />
          <WingBlank>
            <Button
              style={{
                borderColor: '#f1a34f',
                borderRadius: 50,
              }}
              onPress={() => {
                navigation.navigate({
                  routeName: 'ResetPassword',
                });
              }}
            >
              <Text
                style={{
                  color: '#f1a34f',
                }}
              >
                {I18n.t('btn_forgetPassword')}
              </Text>
            </Button>
          </WingBlank>
          <WhiteSpace size="xl" />
          <WhiteSpace size="xl" />
        </KeyboardAvoidingView>
        <ActivityIndicator
          animating={this.state.animating}
          toast
          size="large"
          text={I18n.t('label_loading')}
        />
      </ScrollView>
    );
  }
}

const mapStateToProps = state => state.login;

const mapDispatchToProps = dispatch => {
  return {
    onLogin: payload => {
      dispatch(LoginActions.loginRequest(payload));
    },
    onReset: () => {
      dispatch(LoginActions.reset());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
