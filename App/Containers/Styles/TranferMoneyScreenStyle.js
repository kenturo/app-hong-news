import { StyleSheet } from "react-native";
import { ApplicationStyles, Colors } from "../../Themes/";

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  headerTitle: {
    color: Colors.headerTitleColor,
    fontSize: 30
  },

  iconInput: {
    flex: 1,
    textAlign: "left",
    marginBottom: 5,
    width: "100%",
    marginLeft: 25,
    fontSize: 20,
    color: "#000"
  },
  input: {
    flex: 1,
    borderWidth: 1,
    borderColor: "#d3d8dc",
    borderRadius: 5,
    color: "#000",
    paddingLeft: 10
  },
  txtNote: {
    color: Colors.textColor,
    fontSize: 20,
    marginLeft: 25,
    width: "100%"
  }
});
