import { StyleSheet } from 'react-native';
import { Colors, Metrics } from '../../Themes/';

export default StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: Metrics.navBarHeight,
  },
  wrapHeaderTable: {
    borderWidth: 0.5,
  },
  itemCol: {
    flex: 1,
    borderRightWidth: 0.3,
    textAlign: 'center',
    paddingVertical: 5,
  },
  bgRed: {
    borderColor: '#fff',
    backgroundColor: Colors.MainTopHeader.backgroundColor,
    color: '#fff',
  },
  noBorderTop: {
    borderTopWidth: 0,
  },
  itemPaging: {
    // width: 30,
    // height: 30,
    borderRightWidth: 0.3,
    borderTopWidth: 0.3,
    borderBottomWidth: 0.2,
    textAlign: 'center',
    paddingVertical: 4,
    paddingHorizontal: 10,
    color: Colors.MainTopHeader.backgroundColor,
  },
  itemPrev: {
    borderLeftWidth: 0.3,
  },
  itemNext: {
    borderRightWidth: 0.3,
  },

  itemPagingActive: {
    color: '#fff',
    backgroundColor: Colors.MainTopHeader.backgroundColor,
  },

  txtStyle: {
    flex: 1,
    lineHeight: 40,
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  txtStatus: {
    // width: 20,
    marginTop: 5,
    paddingHorizontal: 6,
    paddingVertical: 4,
    borderRadius: 8,
    textAlign: 'center',
    color: '#fff',
  },

  txtStatusDone: {
    backgroundColor: '#28a745',
  },
  txtStatusFail: {
    backgroundColor: '#dc3545',
  },

  txtStatusBal: {
    backgroundColor: '#ffc107',
  },
  textDes: {
    fontSize: 12,
  },
});
