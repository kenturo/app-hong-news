import { StyleSheet, Dimensions } from "react-native";
// import { Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height - 84
  },
  bar: {
    height: 30
  },
  icoClose: {
    flex: 0,
    marginRight: 10
  },
  address: {
    flex: 3,
    textAlign: "center",
    fontWeight: "bold",
    color: "#00c853",
    textAlignVertical: "center"
  }
});
