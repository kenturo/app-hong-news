import { StyleSheet } from "react-native";
import { ApplicationStyles, Colors } from "../../Themes/";

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  wrap: {
    borderWidth: 1,
    borderColor: Colors.borderColor,
    borderTopWidth: 0,
    borderBottomColor: "#dfdfdf",
    borderRightColor: "#dfdfdf",
    borderRadius: 8
  },
  txtMoney: {
    fontSize: 25,
    color: Colors.textColor
  },
  txtTitle: {
    marginBottom: 10,
    fontSize: 25,
    color: "#000"
  }
});
