import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, View, Text, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import {
  Button,
  Flex,
  WhiteSpace,
  WingBlank,
  InputItem,
  Toast,
  Portal,
} from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';
import { createForm } from 'rc-form';
import TitleScreen from '../Components/TitleScreen';
import BankDepositeListView from '../Components/BankDepositeListView';
import ChannelBankSelectedView from '../Components/ChannelBankSelectedView';
import HelperFunc from '../Lib/helper';
import request from '../Lib/request';
import WalletActions from '../Redux/WalletRedux';
import I18n from '../I18n';
import * as Sentry from '@sentry/react-native';

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/DepositScreenStyle';

/**
 * bank_name: 中国工商银行
account_name: test
account_number: test
transaction_code: 
bank: 0
channel: 0
amount: 50
id: banking => localbank

 */

class DepositScreen extends Component {
  state = {
    errorArr: [],
    isFetching: false,
    bankSelected: { id: 'banking' },
    bankInfo: {},
  };

  componentDidMount() {
    this.getBankInfo();
    this.props.navigation.addListener('didFocus', () => {
      this.getBankInfo();
    });
  }

  onChangeBankSelected = bankSelected => {
    this.setState({ bankSelected, errorArr: [] });
  };

  getBankInfo = () => {
    this.setState({ isFetching: true });
    request.get('/bank_info').then(t => {
      const errorArr = get(t.data, 'messages', null);
      if (t.ok) {
        // check error code
        if (get(t.data, 'error_code', 0) > 0) {
          this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
        }

        this.setState({
          bankInfo: get(t.data, 'data', []),
        });
      } else {
        this.setState({ errorArr });
      }
      this.setState({ isFetching: false });
    });
  };

  renderFormLocalBank = () => {
    const { bankInfo } = this.state;
    const { form } = this.props;
    const { getFieldProps } = form;
    return (
      <View>
        <WhiteSpace size="lg" />
        <WingBlank style={styles.wingBlankStyle}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="institution" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={styles.input}>
              <InputItem
                placeholder={I18n.t('placeHolder_bankName')}
                {...getFieldProps('bank_name', {
                  initialValue: get(bankInfo, 'bank_name', undefined),
                  rules: [
                    {
                      required: true,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_required',
                        'placeHolder_bankName'
                      ),
                    },
                    {
                      min: 1,
                      message: I18n.t('validate_len_bankName'),
                    },
                    {
                      WhiteSpace: false,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_whiteSpace',
                        'placeHolder_bankName'
                      ),
                    },
                  ],
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>

        <WhiteSpace size="lg" />
        <WingBlank style={styles.wingBlankStyle}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="user" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={styles.input}>
              <InputItem
                placeholder={I18n.t('placeHolder_accountName')}
                {...getFieldProps('account_name', {
                  initialValue: get(bankInfo, 'account_name', undefined),
                  rules: [
                    {
                      required: true,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_required',
                        'placeHolder_accountName'
                      ),
                    },
                    {
                      min: 1,
                      message: I18n.t('validate_len_accountName'),
                    },
                    {
                      WhiteSpace: false,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_whiteSpace',
                        'placeHolder_accountName'
                      ),
                    },
                  ],
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>

        <WhiteSpace size="lg" />
        <WingBlank style={styles.wingBlankStyle}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="credit-card" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={styles.input}>
              <InputItem
                placeholder={I18n.t('placeHolder_accountNumber')}
                {...getFieldProps('account_number', {
                  initialValue: get(bankInfo, 'account_number', undefined),
                  rules: [
                    {
                      required: true,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_required',
                        'placeHolder_accountNumber'
                      ),
                    },
                    {
                      min: 1,
                      message: I18n.t('validate_len_accountNumber'),
                    },
                    {
                      WhiteSpace: false,
                      message: HelperFunc.getMesgPlural(
                        'plural_field_whiteSpace',
                        'placeHolder_accountNumber'
                      ),
                    },
                  ],
                })}
              ></InputItem>
            </Flex.Item>
          </Flex>
        </WingBlank>
      </View>
    );
  };

  renderFormAnotherBank = () => {
    const { bankSelected, isFetching } = this.state;
    const { form } = this.props;
    const { getFieldProps } = form;

    const opstions = get(bankSelected, 'options', []);
    const dataSrc = opstions.map((t, k) => ({
      label: `通道${k + 1} (${t.min} 元 - ${t.max} 元)`,
      value: t.value,
    }));

    return (
      <View>
        <WhiteSpace size="lg" />
        <WingBlank style={styles.wingBlankStyle}>
          <Flex justify="start">
            <Flex.Item style={styles.iconInput}>
              <Icon size={25} name="institution" color="#6b6c6e" />
            </Flex.Item>
            <Flex.Item style={styles.input}>
              <ChannelBankSelectedView
                data={dataSrc}
                form={getFieldProps('gate', {
                  initialValue: get(dataSrc, '[0].value', 'Choose'),
                })}
                loading={isFetching}
              />
            </Flex.Item>
          </Flex>
        </WingBlank>
      </View>
    );
  };

  onSubmit = () => {
    const { form, onLoadWalletList } = this.props;
    const { validateFields, getFieldsError } = form;
    const { bankSelected } = this.state;
    validateFields((err, values) => {
      if (!err) {
        this.setState({ errorArr: null });
        const toast = Toast.loading(I18n.t('label_loading'), 0);

        let payload = cloneDeep(
          Object.assign({}, values, {
            id: this.state.bankSelected.id,
            amount: Number(values.amount),
            transaction_code: '',
            bank: 0,
            channel: 0,
          })
        );

        if (bankSelected.id !== 'banking') {
          payload = Object.assign(
            {},
            {
              bank_name: bankSelected.id,
              amount: Number(values.amount),
              id: 'onlinepayemnt',
              gate: values.gate.toString(),
            }
          );
        } else {
          if (Number(values.amount) < 50) {
            Toast.fail(I18n.t('tagline_form_transfer_min'), 1);
            return;
          }
        }

        const fd = new FormData();
        Object.keys(payload).forEach(function(key) {
          fd.append(key, payload[key]);
        });

        // this.setState({ isFetching: true });

        try {
          request.post('/deposit', fd).then(t => {
            const errorArr = get(t.data, 'messages', null);

            if (t.ok) {
              // check error code
              if (get(t.data, 'error_code', 0) > 0) {
                this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
              } else {
                this.setState({
                  errorArr: null,
                });

                // success
                const urlData = get(t.data, 'data', null);
                if (urlData) {
                  HelperFunc.loadInBrowser(urlData);
                } else {
                  Toast.success(get(t.data, 'message', null), 1, () => onLoadWalletList());
                }

                // resetFields(); // resetAll
              }
            } else {
              this.setState({ errorArr });
            }
            Portal.remove(toast);
          });
        } catch (error) {
          Sentry.captureException(new Error(error));
          Portal.remove(toast);
          // console.log('TCL: DepositScreen -> onSubmit -> error', error);
        } finally {
          // Portal.remove(toast);
        }
      } else {
        this.setState({ errorArr: getFieldsError() });
      }
    });
  };

  render() {
    const { errorArr, isFetching, bankSelected } = this.state;
    const { form } = this.props;
    const { getFieldProps } = form;
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior="padding">
          <TitleScreen value={I18n.t('label_deposit')} />

          <WhiteSpace size="xl" />
          {HelperFunc.getErrorFormForShow(errorArr)}

          {get(bankSelected, 'id', null) === 'banking'
            ? this.renderFormLocalBank()
            : this.renderFormAnotherBank()}
          <WhiteSpace size="lg" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="cny" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  type="number"
                  style={styles.input}
                  placeholder={I18n.t('placeHolder_amount')}
                  {...getFieldProps('amount', {
                    rules: [
                      {
                        required: true,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_required',
                          'placeHolder_amount'
                        ),
                      },
                      {
                        WhiteSpace: false,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_whiteSpace',
                          'placeHolder_amount'
                        ),
                      },
                    ],
                    getValueFromEvent: e =>
                      e
                        ? e
                            .replace(/^0/, '')
                            .replace(/[a-zA-Z]\w+/, '')
                            .trim()
                        : e,
                  })}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>
          <WhiteSpace size="lg" />
          <WingBlank>
            <Flex>
              <Text style={styles.txtNote} adjustsFontSizeToFit allowFontScaling>
                {get(bankSelected, 'id', null) === 'banking'
                  ? I18n.t('tagline_form_deposit_part_1')
                  : null}
              </Text>
              <TouchableOpacity
                onPress={() => HelperFunc.loadInBrowser('https://lc.chat/now/10753432/')}
              >
                <Text
                  style={[
                    styles.txtNote,
                    {
                      color: '#007bff',
                    },
                  ]}
                  adjustsFontSizeToFit
                  allowFontScaling
                >
                  {get(bankSelected, 'id', null) === 'banking'
                    ? I18n.t('tagline_form_deposit_part_2')
                    : null}
                </Text>
              </TouchableOpacity>
            </Flex>
          </WingBlank>
          <WhiteSpace size="xl" />

          <WingBlank>
            <Button type="warning" loading={isFetching} onPress={this.onSubmit}>
              {I18n.t('label_deposit')}
            </Button>
          </WingBlank>

          <WhiteSpace />
          <WhiteSpace size="xl" />
          <Flex direction="column">
            <Flex.Item>
              <Text style={[styles.headerTitle]} adjustsFontSizeToFit allowFontScaling>
                {I18n.t('label_otherBank')}
              </Text>
            </Flex.Item>
          </Flex>
          <WhiteSpace />
          <WingBlank>
            <WhiteSpace />
            <BankDepositeListView onChange={this.onChangeBankSelected} />
          </WingBlank>
          <WhiteSpace size="xl" />
          <WhiteSpace size="xl" />
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}

const BingdingForm = createForm()(DepositScreen);

const mapDispatchToProps = dispatch => {
  return {
    onLoadWalletList: () => {
      dispatch(WalletActions.walletRequest());
    },
  };
};

export default connect(null, mapDispatchToProps)(BingdingForm);
