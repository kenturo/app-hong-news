import React, { Component } from 'react';
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native';
import {
  Button,
  Flex,
  WhiteSpace,
  WingBlank,
  SegmentedControl,
  Toast,
} from '@ant-design/react-native';
import { createForm } from 'rc-form';
import HelperFunc from '../Lib/helper';
import FormProfile from '../Components/FormProfile';
import FormBank from '../Components/FormBank';
import FormChangePassword from '../Components/FormChangePassword';
import request from '../Lib/request';
import get from 'lodash/get';
import I18n from '../I18n';
// import ErrorBarInline from '../Components/ErrorBarInline';
// import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/ProfileScreenStyle';

class ProfileScreen extends Component {
  state = {
    currentEndpoint: '/user',
    currentTab: '账号',
    errorArr: [],
    isFetching: false,
    user: {},
    bankInfo: {},
  };

  componentDidMount() {
    this.getProfile();
  }

  getProfile = () => {
    this.setState({ isFetching: true });
    request.get('/user').then(t => {
      // console.log('TCL: ProfileScreen -> getProfile -> t', t);
      const errorArr = get(t.data, 'messages', null);
      if (t.ok) {
        // check error code
        if (get(t.data, 'error_code', 0) > 0) {
          this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
        }

        this.setState({
          user: get(t.data, 'data', []),
        });
      } else {
        this.setState({ errorArr });
      }
      this.setState({ isFetching: false });
    });
  };

  getBankInfo = () => {
    this.setState({ isFetching: true });
    request.get('/bank_info').then(t => {
      // console.log('LOG: -----------------------------------------');
      // console.log('LOG: ProfileScreen -> getBankInfo -> t', t);
      // console.log('LOG: -----------------------------------------');
      const errorArr = get(t.data, 'messages', null);
      if (t.ok) {
        // check error code
        if (get(t.data, 'error_code', 0) > 0) {
          this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
        }

        this.setState({
          bankInfo: get(t.data, 'data', []),
        });
      } else {
        this.setState({ errorArr });
      }
      this.setState({ isFetching: false });
    });
  };

  onChangeTab = currentTab => {
    let currentEndpoint = '/user';
    switch (currentTab) {
      case '银行信息':
        this.getBankInfo();
        currentEndpoint = '/bank_info';
        break;
      case '账号':
        this.getProfile();
        currentEndpoint = '/user';
        break;
      default:
        currentEndpoint = '/change_password';
        break;
    }

    this.setState({ currentTab, currentEndpoint, errorArr: [] });
  };

  onReload = () => {
    switch (this.state.currentTab) {
      case '银行信息':
        this.getBankInfo();
        break;
      case '账号':
        this.getProfile();
        break;
      default:
        this.props.form.resetFields();
        break;
    }
  };

  RenderFormByTab = ({ form }) => {
    const { currentTab, user, bankInfo } = this.state;
    switch (currentTab) {
      case '账号':
        return <FormProfile form={form} user={user} />;
      case '银行信息':
        return <FormBank form={form} data={bankInfo} />;
      default:
        return <FormChangePassword form={form} />;
    }
  };

  onSubmit = () => {
    const { validateFields, getFieldsError } = this.props.form;

    validateFields((err, payload) => {
      if (!err) {
        this.setState({ errorArr: null });

        // console.log('payload', payload);
        // const fd = new FormData();
        // Object.keys(payload).forEach(function(key) {
        //   fd.append(key, payload[key]);
        // });

        this.setState({ isFetching: true });

        request.put(this.state.currentEndpoint, payload).then(t => {
          // console.log('LOG: ---------------------');
          // console.log('LOG: onSubmit -> t', t);
          // console.log('LOG: ---------------------');
          const errorArr = get(t.data, 'messages', null);

          if (t.ok) {
            // check error code
            if (get(t.data, 'error_code', 0) > 0) {
              this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
            } else {
              this.setState({
                errorArr: null,
              });
              // success
              Toast.success(get(t.data, 'message', null), 1, () => this.onReload());
            }
          } else {
            this.setState({ errorArr });
          }
          setTimeout(() => {
            this.setState({ isFetching: false });
          }, 2000);
        });
      } else {
        this.setState({ errorArr: getFieldsError() });
      }
    });
  };

  render() {
    const { errorArr, isFetching } = this.state;
    const { form } = this.props;
    // const { getFieldProps, validateFields, getFieldValue, getFieldsError } = form;

    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior="padding">
          <WhiteSpace size="xl" />
          <Flex direction="column">
            <Flex.Item>
              <Text style={styles.headerTitle} adjustsFontSizeToFit allowFontScaling>
                {this.state.currentTab}
              </Text>
            </Flex.Item>
          </Flex>
          <WhiteSpace />
          <WingBlank>
            <SegmentedControl
              values={['账号', '银行信息', '换密码']}
              onValueChange={this.onChangeTab}
            />
          </WingBlank>
          <WhiteSpace />
          {HelperFunc.getErrorFormForShow(errorArr)}
          <WhiteSpace />

          <this.RenderFormByTab form={form} />

          <WhiteSpace size="xl" />
          <WingBlank>
            <Button type="warning" onPress={this.onSubmit} loading={isFetching}>
              {I18n.t('label_profile')}
            </Button>
          </WingBlank>
          <WhiteSpace size="xl" />
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}

export default createForm()(ProfileScreen);
