import React, { useState } from 'react';
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import { Button, Flex, WhiteSpace, WingBlank, InputItem } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createForm } from 'rc-form';
import get from 'lodash/get';
import * as Sentry from '@sentry/react-native';

import request from '../Lib/request';

import HelperFunc from '../Lib/helper';
import Loading from '../Components/Loading';

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import LoginActions from '../Redux/LoginRedux';
import I18n from '../I18n';

// Styles
import styles from './Styles/RegisterScreenStyle';

/*
/register
username: Ken123
password: asd@123
confirm_password: asd@123
email: nah82063@eveav.com
mobile: 0908789876
*/

const RegisterScreen = props => {
  const { getFieldProps, validateFields, getFieldValue, getFieldsError, resetFields } = props.form;
  const { navigation, setTokenApp } = props;

  const [errorArr, setErrorArr] = useState({});
  const [isFetching, setFetching] = useState(false);

  const onSubmitForm = () => {
    validateFields((err, value) => {
      if (!err) {
        setErrorArr({});
        try {
          //ss
          setFetching(true);
          const fd = new FormData();
          Object.keys(value).forEach(function(key) {
            fd.append(key, value[key]);
          });
          request.post('/register', fd).then(t => {
            const errorMesg = get(t.data, 'messages', null);

            if (t.ok) {
              // check error code
              if (get(t.data, 'error_code', 0) > 0) {
                setErrorArr(errorMesg);
              } else {
                // success

                setTokenApp(get(t.data, 'data', null));

                setTimeout(() => {
                  setErrorArr({});
                  resetFields();
                  navigation.navigate({
                    routeName: 'HomeTab',
                  });
                }, 1000);
              }
            } else {
              setErrorArr(errorMesg);
            }
            setTimeout(() => {
              setFetching(false);
            }, 2000);
          });
        } catch (error) {
          Sentry.captureException(new Error(error));
        } finally {
          setFetching(false);
        }
      } else {
        setErrorArr(getFieldsError());
      }
    });
  };

  navigation.addListener('didFocus', () => {
    setErrorArr({});
  });

  const compareToFirstPassword = (rule, value, callback) => {
    if (value && value !== getFieldValue('password')) {
      callback(I18n.t('validate_two_password'));
    } else {
      callback();
    }
  };

  return (
    <ScrollView style={styles.container}>
      <KeyboardAvoidingView behavior="padding">
        <Loading loading={isFetching}>
          <WhiteSpace size="xl" />
          <Flex direction="column">
            <Flex.Item>
              <Text style={styles.headerTitle} adjustsFontSizeToFit allowFontScaling>
                {I18n.t('label_register')}
              </Text>
            </Flex.Item>
          </Flex>
          <WhiteSpace size="xl" />
          {HelperFunc.getErrorFormForShow(errorArr)}
          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="user" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  placeholder={I18n.t('placeHolder_username')}
                  {...getFieldProps('username', {
                    rules: [
                      {
                        required: true,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_required',
                          'label_username'
                        ),
                      },
                      {
                        min: 6,
                        message: I18n.t('validate_len_username'),
                      },
                      {
                        WhiteSpace: false,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_whiteSpace',
                          'label_username'
                        ),
                      },
                    ],
                    getValueFromEvent: v =>
                      v && v.length ? v.toLocaleLowerCase().replace(/\s/g, '') : undefined,
                  })}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>
          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="lock" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  type="password"
                  placeholder={I18n.t('placeHolder_password')}
                  {...getFieldProps('password', {
                    rules: [
                      {
                        required: true,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_required',
                          'label_password'
                        ),
                      },
                      {
                        min: 6,
                        message: I18n.t('validate_len_password'),
                      },
                      {
                        WhiteSpace: false,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_whiteSpace',
                          'label_password'
                        ),
                      },
                    ],
                  })}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>
          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="lock" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  type="password"
                  placeholder={I18n.t('placeHolder_confirmPassword')}
                  {...getFieldProps('confirm_password', {
                    rules: [
                      {
                        required: true,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_required',
                          'label_password'
                        ),
                      },
                      { validator: compareToFirstPassword },
                    ],
                  })}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>
          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="envelope" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  placeholder={I18n.t('placeHolder_email')}
                  {...getFieldProps('email', {
                    rules: [
                      {
                        required: true,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_required',
                          'placeHolder_email'
                        ),
                      },
                      {
                        WhiteSpace: false,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_whiteSpace',
                          'placeHolder_email'
                        ),
                      },
                      { validator: HelperFunc.validateEmail },
                    ],
                    getValueFromEvent: v => (v && v.length ? v.replace(/\s/g, '') : undefined),
                  })}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>
          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={40} name="mobile" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  pattern="[0-9]*"
                  type="number"
                  placeholder={I18n.t('placeHolder_mobile')}
                  {...getFieldProps('mobile', {
                    rules: [
                      {
                        required: true,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_required',
                          'placeHolder_mobile'
                        ),
                      },
                      {
                        min: 9,
                        message: I18n.t('validate_len_mobile'),
                      },
                      {
                        WhiteSpace: false,
                        message: HelperFunc.getMesgPlural(
                          'plural_field_whiteSpace',
                          'placeHolder_mobile'
                        ),
                      },
                    ],
                    getValueFromEvent: v => (v && v.length ? v.replace(/\s/g, '') : undefined),
                  })}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>
          <WhiteSpace size="xl" />
          <WingBlank>
            <Button type="warning" disabled={!errorArr} onPress={onSubmitForm}>
              {I18n.t('btn_register')}
            </Button>
          </WingBlank>
          <WhiteSpace size="xl" />
          <WingBlank>
            <Text
              style={{
                textAlign: 'center',
              }}
            >
              {I18n.t('tagline_form_register')}
            </Text>
          </WingBlank>
          <WhiteSpace size="xl" />
        </Loading>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    setTokenApp: token => {
      dispatch(LoginActions.setToken(token));
    },
  };
};
const connectForm = createForm()(RegisterScreen);

export default connect(null, mapDispatchToProps)(connectForm);
