import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native';
import {
  Button,
  Flex,
  WhiteSpace,
  WingBlank,
  InputItem,
  Toast,
  Portal,
} from '@ant-design/react-native';
import { createForm } from 'rc-form';
import get from 'lodash/get';
import SingleSelectableView from '../Components/SingleSelectableView';
import TitleScreen from '../Components/TitleScreen';
import request from '../Lib/request';
import HelperFunc from '../Lib/helper';
import WalletActions from '../Redux/WalletRedux';
import I18n from '../I18n';
import * as Sentry from '@sentry/react-native';

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/TranferMoneyScreenStyle';

class TranferMoneyScreen extends Component {
  state = {
    errorArr: [],
    isFetching: false,
  };

  componentDidMount() {
    this.getWalletList();
  }

  getWalletList = () => {
    const { onLoadWalletList } = this.props;
    onLoadWalletList();
  };

  onSubmit = () => {
    const { validateFields, getFieldsError } = this.props.form;
    validateFields((err, value) => {
      if (!err) {
        this.setState({ errorArr: null });
        // const payload = Object
        if (value.wallet_from.toString() === 'sportbook') {
          if (Number(value.amount) > 50000) {
            Toast.fail(I18n.t('tagline_form_transfer_max'), 1);
            return;
          }
          if (Number(value.amount) < 50) {
            Toast.fail(I18n.t('tagline_form_transfer_min'), 1);
            return;
          }
        }

        const payload = Object.assign({}, value, {
          wallet_from: value.wallet_from.toString(),
          wallet_to: value.wallet_to.toString(),
          amount: Number(value.amount),
        });

        const fd = new FormData();
        Object.keys(payload).forEach(function(key) {
          fd.append(key, payload[key]);
        });

        const toast = Toast.loading(I18n.t('label_loading'), 0);

        try {
          request.post('/transfer', fd).then(t => {
            const errorArr = get(t.data, 'messages', null);

            if (t.ok) {
              // check error code
              if (get(t.data, 'error_code', 0) > 0) {
                this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
              } else {
                this.setState({
                  errorArr: null,
                });
                Toast.success(get(t.data, 'message', null), 1, () => this.getWalletList());
              }
            } else {
              this.setState({ errorArr });
            }
            Portal.remove(toast);
          });
        } catch (error) {
          Sentry.captureException(new Error(error));
          Portal.remove(toast);
        }
      } else {
        this.setState({ errorArr: getFieldsError() });
      }
    });
  };

  render() {
    const { errorArr } = this.state;
    const { form, walletList, fetching } = this.props;
    const { getFieldProps, getFieldValue } = form;
    const dataSrcSelectable = walletList.map(t => ({
      label: `${t.name} - ¥${t.amount}`,
      value: t.code,
    }));

    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior="padding">
          <TitleScreen value={I18n.t('label_withdraw')} />

          <WhiteSpace size="xl" />
          {HelperFunc.getErrorFormForShow(errorArr)}
          <WhiteSpace size="xl" />
          <Flex direction="column">
            <Text style={styles.iconInput}>{I18n.t('label_walletFrom')}</Text>
            <SingleSelectableView
              data={dataSrcSelectable}
              form={getFieldProps('wallet_from', {
                initialValue: 'main_wallet',
              })}
              loading={fetching}
            />
          </Flex>
          <WhiteSpace size="xl" />
          <Flex direction="column">
            <Text style={styles.iconInput}>{I18n.t('label_walletTo')}</Text>
            <SingleSelectableView
              data={dataSrcSelectable}
              form={getFieldProps('wallet_to', {
                initialValue: 'main_wallet',
              })}
              loading={fetching}
            />
          </Flex>
          <WhiteSpace size="xl" />
          <Flex direction="column">
            <Text style={styles.iconInput}>{I18n.t('label_amount')}</Text>
            <InputItem
              type="number"
              style={styles.input}
              placeholder={I18n.t('placeHolder_amount')}
              {...getFieldProps('amount', {
                rules: [
                  {
                    required: true,
                    message: HelperFunc.getMesgPlural(
                      'plural_field_required',
                      'placeHolder_amount'
                    ),
                  },
                  {
                    WhiteSpace: false,
                    message: HelperFunc.getMesgPlural(
                      'plural_field_whiteSpace',
                      'placeHolder_amount'
                    ),
                  },
                ],
                getValueFromEvent: e =>
                  e
                    ? e
                        .replace(/^0/, '')
                        .replace(/[a-zA-Z]\w+/, '')
                        .trim()
                    : e,
              })}
            ></InputItem>
            <Text style={styles.txtNote}>
              {getFieldValue('wallet_from') === 'sportbook'
                ? I18n.t('tagline_form_transfer_max')
                : null}
            </Text>
          </Flex>
          <WhiteSpace size="xl" />
          <WingBlank>
            <Button type="warning" onPress={this.onSubmit} loading={fetching}>
              {I18n.t('btn_withdraw')}
            </Button>
          </WingBlank>
          <WhiteSpace size="xl" />
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  walletList: state.wallet.data,
  error: state.wallet.error,
  fetching: state.wallet.fetching,
});

const mapDispatchToProps = dispatch => {
  return {
    onLoadWalletList: () => {
      dispatch(WalletActions.walletRequest());
    },
  };
};
const BindingForm = createForm()(TranferMoneyScreen);
export default connect(mapStateToProps, mapDispatchToProps)(BindingForm);
