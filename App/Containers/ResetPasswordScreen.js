import React, { Component } from 'react';
import { ScrollView, Text, KeyboardAvoidingView } from 'react-native';
import {
  Button,
  Flex,
  WhiteSpace,
  WingBlank,
  InputItem,
  ActivityIndicator,
  Toast,
} from '@ant-design/react-native';
import get from 'lodash/get';
import HelperFunc from '../Lib/helper';
import Icon from 'react-native-vector-icons/FontAwesome';
import request from '../Lib/request';
import I18n from '../I18n';
// import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/ResetPasswordScreenStyle';

class ResetPasswordScreen extends Component {
  state = {
    email: null,
    error: null,
    animating: false,
  };

  onSubmit = () => {
    const { email } = this.state;

    if (email && email.length) {
      if (email && HelperFunc.regexValidEmail(email)) {
        try {
          //ss
          this.setState({ animating: true });
          const fd = new FormData();
          fd.append('email', email);

          request.post('/new_password', fd).then(t => {
            const errorMesg = get(t.data, 'message', null);

            if (t.ok) {
              // check error code
              if (get(t.data, 'error_code', 0) > 0) {
                this.setState({
                  error: errorMesg,
                });
              } else {
                this.setState({
                  error: null,
                });
                // success
                Toast.success(errorMesg, 2, () =>
                  this.setState({
                    email: null,
                  })
                );
              }
            } else {
              this.setState({
                error: errorMesg,
              });
            }
          });
        } catch (error) {
          console.assert(error);
        } finally {
          setTimeout(() => {
            this.setState({ animating: false });
          }, 2000);
        }
      } else {
        this.setState({
          error: I18n.t('validate_format_email'),
        });
      }
    } else {
      this.setState({
        error: HelperFunc.getMesgPlural('plural_field_required', 'placeHolder_email'),
      });
    }
  };

  onSetEmailField = e => {
    if (e) {
      this.setState({
        email: e.replace(/\s/g, ''),
      });
    }
  };

  render() {
    const { error, email, animating } = this.state;

    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior="padding">
          <WhiteSpace size="xl" />
          <Flex direction="column">
            <Flex.Item>
              <Text style={styles.headerTitle} adjustsFontSizeToFit allowFontScaling>
                {I18n.t('label_resetPassword')}
              </Text>
            </Flex.Item>
          </Flex>

          <WhiteSpace size="xl" />
          {HelperFunc.getErrorFormForShow(error)}
          <WhiteSpace size="xl" />
          <WingBlank style={styles.wingBlankStyle}>
            <Flex justify="start">
              <Flex.Item style={styles.iconInput}>
                <Icon size={25} name="envelope" color="#6b6c6e" />
              </Flex.Item>
              <Flex.Item style={styles.input}>
                <InputItem
                  value={email}
                  onChange={this.onSetEmailField}
                  placeholder={I18n.t('placeHolder_email')}
                ></InputItem>
              </Flex.Item>
            </Flex>
          </WingBlank>

          <WhiteSpace size="xl" />
          <WingBlank>
            <Button loading={animating} type="warning" onPress={() => this.onSubmit()}>
              {I18n.t('btn_send')}
            </Button>
          </WingBlank>
          <WhiteSpace size="xl" />
        </KeyboardAvoidingView>
        <ActivityIndicator
          animating={this.state.animating}
          toast={true}
          size="large"
          text={I18n.t('label_loading')}
        />
      </ScrollView>
    );
  }
}

export default ResetPasswordScreen;
