import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import MainBanner from '../Components/MainBanner';
import styles from './Styles/LaunchScreenStyles';

class PromotionScreen extends Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.mainContainer}>
          <MainBanner renderRaw={true} />
        </View>
      </ScrollView>
    );
  }
}

export default PromotionScreen;
