import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, Text, View, Dimensions } from 'react-native';
import { WingBlank, WhiteSpace, Button, Flex } from '@ant-design/react-native';
import { createForm } from 'rc-form';
import qs from 'querystringify';
import moment from 'moment';
import request from '../Lib/request';
import get from 'lodash/get';
import find from 'lodash/find';

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import TitleScreen from '../Components/TitleScreen';
import TableList from '../Components/TableList';
import SearchHistories from '../Components/SearchHistories';
import I18n from '../I18n';
import WalletActions from '../Redux/WalletRedux';
// Styles
import styles from './Styles/HistoryDepositScreenStyle';

const wCell = Dimensions.get('window').width / 4;

class HistoryTransferScreen extends Component {
  state = {
    data: [],
    isFetching: false,
    errorArr: [],
    paging: {},
    query: {},
  };

  componentDidMount() {
    this.props.onLoadWalletList();
  }

  rowDisplay = [
    {
      key: 'id',
      render: r => <Text style={styles.txtStyle}>#{r}</Text>,
    },
    {
      key: 'status',
      render: r => {
        let text = null;
        let styleTxt = 'txtStatusBal';

        switch (r) {
          case 'CANCELED':
            text = '已取消'; //'LOSE';
            styleTxt = 'txtStatusFail';
            break;

          case 'DONE':
            text = '完成'; //'WON';
            styleTxt = 'txtStatusDone';
            break;

          default:
            text = '处理中'; //'DRAW';
            styleTxt = 'txtStatusBal';
            break;
        }

        return (
          <View style={styles.txtStyle}>
            <Text style={[styles.txtStatus, styles[styleTxt]]}>{text}</Text>
          </View>
        );
      },
    },
    {
      key: 'from_wallet',
      render: r => {
        const { walletList } = this.props;
        const item = find(walletList, { id: r });
        return <Text style={styles.txtStyle}>{get(item, 'name', r)}</Text>;
      },
    },
    {
      key: 'to_wallet',
      render: r => {
        const { walletList } = this.props;
        const item = find(walletList, { id: r });
        return <Text style={styles.txtStyle}>{get(item, 'name', r)}</Text>;
      },
    },
    {
      key: 'amount',
      render: r => <Text style={[styles.txtStyle]}>{`¥${r}`}</Text>,
    },
    {
      key: 'created_at',
      render: r => (
        <Text
          style={[
            styles.txtStyle,
            {
              lineHeight: 20,
            },
          ]}
        >
          {moment(r).format('hh:mm YYYY/MM/DD')}
        </Text>
      ),
    },
  ];

  getData = nextquery => {
    this.setState({ isFetching: true });
    const mergeQ = Object.assign({}, this.state.query, nextquery);
    this.setState({ query: mergeQ });
    const qr = qs.stringify(mergeQ);
    request.get(`/history/transfer?${qr}`).then(t => {
      // console.log('TCL: HistoryTransferScreen -> getData -> t', t);
      const errorArr = get(t.data, 'messages', null);
      if (t.ok) {
        // check error code
        if (get(t.data, 'error_code', 0) > 0) {
          this.setState({ errorArr: errorArr || get(t.data, 'message', null) });
        }

        const { data, ...rest } = get(t.data, 'data', {});
        this.setState({
          data,
          paging: rest,
        });
      } else {
        this.setState({ errorArr });
      }
      this.setState({ isFetching: false });
    });
  };

  onSubmit = () => {
    const { validateFields } = this.props.form;
    validateFields((err, payload) => {
      this.getData({
        ...payload.time,
        game: payload.game,
      });
    });
  };

  render() {
    const { isFetching, data, paging } = this.state;
    const { form } = this.props;
    const { getFieldDecorator, setFieldsValue } = form;
    getFieldDecorator('time');

    return (
      <ScrollView style={styles.container}>
        <TitleScreen value={I18n.t('label_history_deposit')} />
        <WhiteSpace size="xl" />
        <WingBlank>
          <SearchHistories onChange={v => setFieldsValue({ time: v })} />
          <WhiteSpace />
          <Flex justify="end">
            <Button
              type="warning"
              onPress={this.onSubmit}
              loading={isFetching}
              style={{
                width: 100,
                height: 35,
              }}
            >
              {I18n.t('btn_search')}
            </Button>
          </Flex>
        </WingBlank>
        <WhiteSpace size="xl" />
        <WingBlank>
          <TableList
            width={wCell * 6}
            columnsHead={['编号', '状态', '从钱包', '至钱包', '金额', '日期']}
            columnData={this.rowDisplay}
            widthArr={new Array(6).fill(wCell)}
            heightArr={new Array(6).fill(40)}
            isFetching={isFetching}
            data={data}
            paging={paging}
            onPagingChange={this.getData}
          />
        </WingBlank>
      </ScrollView>
    );
  }
}
const mapStateToProps = state => ({
  walletList: state.wallet.data,
  error: state.wallet.error,
  fetching: state.wallet.fetching,
});

const mapDispatchToProps = dispatch => {
  return {
    onLoadWalletList: () => {
      dispatch(WalletActions.walletRequest());
    },
  };
};
const BindingForm = createForm()(HistoryTransferScreen);
export default connect(mapStateToProps, mapDispatchToProps)(BindingForm);
