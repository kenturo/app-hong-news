import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import { connect } from 'react-redux';
import { WhiteSpace } from '@ant-design/react-native';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import get from 'lodash/get';
import CategoriesCardView from '../Components/CategoriesCardView';
import TitleScreen from '../Components/TitleScreen';
// Styles
import styles from './Styles/CategoriesDetailListScreenStyle';
import { TITLE_GAME_BY_TYPE } from '../Constants';

class CategoriesDetailListScreen extends Component {
  getTypeInParams = params => {
    if (params && params.type.includes('top')) {
      return params.type.substr(4);
    } else {
      return params.type;
    }
  };

  render() {
    const { navigation, token } = this.props;
    const params = get(navigation, 'state.params', null);
    const type = this.getTypeInParams(params);

    return (
      <ScrollView style={styles.container}>
        <TitleScreen value={get(params, 'titleScreen', TITLE_GAME_BY_TYPE[params.type])} />
        <WhiteSpace size="xl" />
        <View style={styles.mainContainer}>
          <CategoriesCardView
            token={token}
            hiddenBorder={true}
            navigation={navigation}
            type={type}
          />
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  token: get(state.login, 'token', null),
});
export default connect(mapStateToProps, null)(CategoriesDetailListScreen);
