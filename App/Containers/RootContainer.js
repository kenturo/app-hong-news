import React, { Component } from 'react';
import { View, StatusBar } from 'react-native';
import { Provider } from '@ant-design/react-native';
import ReduxNavigation from '../Navigation/ReduxNavigation';
import { connect } from 'react-redux';
import StartupActions from '../Redux/StartupRedux';
import ReduxPersist from '../Config/ReduxPersist';
// import ModalWebview from "./ModalWebview";
// Styles
import styles from './Styles/RootContainerStyles';

class RootContainer extends Component {
  componentDidMount() {
    // if redux persist is not active fire startup action
    if (!ReduxPersist.active) {
      this.props.startup();
    }
  }

  render() {
    return (
      <Provider>
        <View style={styles.applicationView}>
          <StatusBar barStyle="light-content" />
          <ReduxNavigation />
        </View>
      </Provider>
    );
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup()),
});

export default connect(null, mapDispatchToProps)(RootContainer);
