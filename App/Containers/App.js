import '../Config';
import DebugConfig from '../Config/DebugConfig';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import RootContainer from './RootContainer';
import createStore from '../Redux';
import firebase from '@react-native-firebase/app';
import '@react-native-firebase/analytics';
import * as Sentry from '@sentry/react-native';

Sentry.init({
  dsn: 'https://4e0810af62f34f42a7a4fcfdb5914966@sentry.io/1873384',
});

// create our store
const store = createStore();

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
class App extends Component {
  trackScreenView = async screen => {
    // Set & override the MainActivity screen name
    await firebase.analytics().setCurrentScreen(screen, screen);
  };

  componentDidMount() {
    firebase.analytics().setAnalyticsCollectionEnabled(true);
    this.trackScreenView('containerApp');
    firebase.analytics().logEvent('containerApp');
  }

  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    );
  }
}

// allow reactotron overlay for fast design in dev mode
export default DebugConfig.useReactotron ? console.tron.overlay(App) : App;
