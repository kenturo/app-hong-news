import { Platform } from 'react-native';
import firebase from '@react-native-firebase/app';

// pluck values from your `GoogleService-Info.plist` you created on the firebase console
const iosConfig = {
  clientId: '721405648703-kkhc2usd7m8nlsk0jjp0cturluuis6pk.apps.googleusercontent.com',
  appId: '1:721405648703:android:0d99919ce5aa37f99a3d8a',
  apiKey: 'AIzaSyArN99Uo-Ho6wy5fL_OlESXW6PFVGWJCbY',
  storageBucket: 'mobile-app-9de4e.appspot.com',
  projectId: 'mobile-app-9de4e',
  databaseURL: 'x',
  messagingSenderId: 'x',
  // enable persistence by adding the below flag
  persistence: true,
};

// pluck values from your `google-services.json` file you created on the firebase console
const androidConfig = {
  clientId: '721405648703-kkhc2usd7m8nlsk0jjp0cturluuis6pk.apps.googleusercontent.com',
  appId: '1:721405648703:android:0d99919ce5aa37f99a3d8a',
  apiKey: 'AIzaSyArN99Uo-Ho6wy5fL_OlESXW6PFVGWJCbY',
  storageBucket: 'mobile-app-9de4e.appspot.com',
  projectId: 'mobile-app-9de4e',
  databaseURL: '',
  messagingSenderId: '',

  // enable persistence by adding the below flag
  persistence: true,
};

try {
  if (firebase.app('[DEFAULT]') === undefined) {
  }
} catch (error) {
  firebase.initializeApp(
    // use platform-specific firebase config
    Platform.OS === 'ios' ? iosConfig : androidConfig,
    // name of this app
    '[DEFAULT]'
  );
}
