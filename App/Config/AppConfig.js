// Simple React Native specific changes

import "../I18n/I18n";

export default {
  // font scaling override - RN default is on
  allowTextFontScaling: true,
  HOSTNAME_ENDPOINT: "https://008686.com"
};
