// @flow

import I18n from 'react-native-i18n';

I18n.locale = 'zh';
// Enable fallbacks if you want `en-US` and `en-GB` to fallback to `en`
I18n.fallbacks = true;

// English language is the main language for fall back:
I18n.translations = {
  zh: require('./languages/zh.json'),
  en: require('./languages/english.json'),
};

let languageCode = I18n.locale.substr(0, 2);

// All other translations for the app goes to the respective language file:
switch (languageCode) {
  case 'en':
    I18n.translations.en = require('./languages/english.json');
    break;
  case 'zh':
    I18n.translations.zh = require('./languages/zh.json');
    break;
}
